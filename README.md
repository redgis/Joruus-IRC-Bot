# Joruus irC Bot

## What is Joruus irC Bot ?

Joruus irCBot is an IRC bot (short name for "robot") developped in C++ and will probably be scriptable in LUA. For noobs and Human Resources : "IRC" is a system allowing people to chat online (IRC stands for Internet Relay Chat). When you join a chat room [like this for exemple](http://xchat.org/files/screenshots/xc-combined.png), you are in fact using an IRC server. "Bots" (i.e.: robots) are virtual chater connected to the room for various purpose : administration, authority, artificial discussion, etc. IRC is not only a supercicial discussion tool for teenagers, it has also been a powerful support for developping communities, and that is my interest here.

## UML

When I start a project, I am usually determined to keep things clear so that I will always be able to leave the project for a while, and come back to it few week later, for example. So I am fond of clean code with meaningful comments, and above all I venerate the "KISS" proverb : Keep It Stupid, Simple. This means in general, the simplest solution is the best. At least in computer science ... in small projects like this one. Any way, I started this project with a simple UML class-diagram using the _Umbrello_ UML modeler (see "first diagram"). I than started developping Joruus and ended up with a working program managing IRC communications via events. I recently updated the first class diagramm to make things clear on the changes I had to do during developpment. I used Objecteering UML Modeler, and a good thing is my original class diagramm does not differ too much from this "after-developpment" version (see "second diagram").

First UML classe diagram:
![First UML classe diagram](Modele/diagramme%20de%20classes.png)

Second UML classe diagram:
![Second UML classe diagram](Modele/diagramme-classes.jpg)

I have used the IRC standard specifications to define what communication Joruus should be able to handle. Not all events are handled right now (still under developmement). You will find the [RFC standards documents](Doc/) I am using.

## Events handling and scripting

The interest of Joruus is that it is cleanly developped, simple to use, and thus easy to debug. Due to its event-driven architecture, it will also be very easy to programm plugins, or addons. Basically, to handle any IRC communication, the programmer only need to define/redefine (if one want to modify the default behavior) a new event, and the associated handler function.

More details to come here, when I will developp a plugin / script interface, using LUA scripting language.

Below is a screenshot of the consol log of Joruus.

![](Modele/shot1.jpg)


 --- 2010/03/29