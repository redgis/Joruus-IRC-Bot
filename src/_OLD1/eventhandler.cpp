/***************************************************************************
 *            eventhandler.cpp
 *
 *  Mon Feb  17 16:17:03 2008
 *  Copyright  2007  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "eventhandler.h"

/***************************************************************************/
Joruus::CEventHandler::CEventHandler (THandler newHdl) : Handler(newHdl)
{
}


/***************************************************************************/
Joruus::CEventHandler::~CEventHandler ()
{
}


/***************************************************************************/
void Joruus::CEventHandler::setHandler ( THandler newHdl )
{
  Handler = newHdl;
}


/***************************************************************************/
THandler Joruus::CEventHandler::getHandler ()
{
  return Handler;
}


/***************************************************************************/
int Joruus::CEventHandler::RunHandler (Joruus::TEvent * evt)
{
  if (Handler != nullptr)
  {
    char * TimeTmp = ctime(& (evt->Timestamp));
    TimeTmp[strlen(TimeTmp)-1] = '\0';

    cout << "Event recieved : {" << evt->Keyword << "}" << endl;
    cout << "            at : {" << TimeTmp << "}" << endl;
    cout << "          from : {" << evt->Prefix << "}" << endl;
    cout << "               : {" << evt->Data << "}" << endl;
    return (*Handler) (evt);
  }
  else
    return -1;
}

