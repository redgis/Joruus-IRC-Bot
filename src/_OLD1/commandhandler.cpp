/***************************************************************************
 *            eventhandler.cpp
 *
 *  Mon Feb  17 16:17:03 2008
 *  Copyright  2007  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "eventhandler.h"

/***************************************************************************/
Joruus::CCommandHandler::CEventHandler (THandler newHdl) : Handler(newHdl)
{
}


/***************************************************************************/
Joruus::CCommandHandler::~CCommandHandler ()
{
}


/***************************************************************************/
void Joruus::CCommandHandler::setHandler ( TCommandHandler newHdl )
{
  m_Handler = newHdl;
}


/***************************************************************************/
THandler Joruus::CCommandHandler::getHandler ()
{
  return m_Handler;
}


/***************************************************************************/
int Joruus::CCommandHandler::RunHandler (string & arguments)
{
  if (m_Handler != nullptr)
  {

    return (*m_Handler) (arguments);
  }
  else
    return -1;
}

