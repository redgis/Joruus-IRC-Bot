/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _NETWORK_H_
#define _NETWORK_H_

extern "C" {

/*****************************************************************************
 * Resolution DNS : on veut l'IP en fonction du parametre en chaine de car.
 * (a nous de trouver si c'est une IP ou un DNS)
 *     unsigned long int GetIP (char * Host);
 * 
 * Resolution DNS inverse : on veut le nom de domaine en fonction de l'IP.
 *     char * GetDNS (unsigned long int AdrIp);
 ****************************************************************************/


/*****************************************************************************/
/* resolution DNS : on veut l'IP en fonction du parametre en chaine de car.
 * (a nous de trouver si c'est une IP ou un DNS) */
unsigned long int GetIP (const char * Host);


/*****************************************************************************/
/* resolution DNS inverse : on veut le nom de domaine en fonction de l'IP */
char * GetDNS (unsigned long int AdrIp);

/*****************************************************************************/
/* TCP connect to a listening IP address / port. Returns the socket */
int TCP_Connect_to (unsigned long int AdrIP, unsigned short int Port);


/*****************************************************************************/
/* User should not use this */
/* We create a local structure to be ready to create an UDP socket to be able to send UDP data. */
int UDP_Init_Socket ();

/*****************************************************************************/
/* Ouvre la socket serveur en ecoute TCP */
int TCP_Open_Listening (unsigned short int Port, unsigned short int * PortOuvert);

/*****************************************************************************/
/* Ouvre la socket serveur en ecoute UDP */
int UDP_Open_Listening (unsigned short int Port, unsigned short int * PortOuvert);

/*****************************************************************************/
int TCP_Send_Message (int Socket, const char * Message);

/*****************************************************************************/
int UDP_Send_Message (unsigned long int AdrIP, unsigned short int Port, const char * Message);

} /*  extern "C"  */

#endif /* _NETWORK_H_ */
