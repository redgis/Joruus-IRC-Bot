/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "user.hpp"


/***************************************************************************/
Joruus::CUser::CUser (CServer * server, string nick, string name, string domain, string realname) : m_Server(server),m_Nickname(nick),m_Name(name),m_Domain(domain),m_RealName(realname)
{
  m_IdleTime = -1;
  m_IrcOp = UNKNOWN;
  m_ChanList.clear ();
}

/***************************************************************************/
Joruus::CUser::~CUser ()
{
  
}

/***************************************************************************/
void Joruus::CUser::setServer (CServer * s)
{
  m_Server = s;
}

/***************************************************************************/
void Joruus::CUser::setNick (string & n)
{
  m_Nickname = n;
}

/***************************************************************************/
void Joruus::CUser::setName (string & n)
{
  m_Name = n;
}

/***************************************************************************/
void Joruus::CUser::setDomain (string & d)
{
  m_Domain = d;
}

/***************************************************************************/
void Joruus::CUser::setRealName (string &rn)
{
  m_RealName = rn;
}

/***************************************************************************/
void Joruus::CUser::setIdleTime (time_t t)
{
  m_IdleTime = t;
}

/***************************************************************************/
void Joruus::CUser::setIrcOp (trilean t)
{
  m_IrcOp = t;
}

/***************************************************************************/
void CUser::setChanStatus (string & c, char s)
{
  m_ChanList[c] = s;
}

/***************************************************************************/
void Joruus::CUser::clearChans ()
{
  m_ChanList.empty();
}

/***************************************************************************/
CServer * Joruus::CUser::getServer ()
{
  return m_Server;
}

/***************************************************************************/
string & Joruus::CUser::getNick ()
{
  return m_Nickname;
}

/***************************************************************************/
string & Joruus::CUser::getName ()
{
  return m_Name;
}

/***************************************************************************/
string & Joruus::CUser::getDomain ()
{
  return m_Domain;
}

/***************************************************************************/
string & Joruus::CUser::getRealName ()
{
  return m_RealName;
}

/***************************************************************************/
time_t Joruus::CUser::getIdleTime ()
{
  return m_IdleTime;
}

/***************************************************************************/
trilean Joruus::CUser::getIrcOp ()
{
  return m_IrcOp;
}

/***************************************************************************/
map<string,char> & Joruus::CUser::getChanList ()
{
  return m_ChanList;
}

/***************************************************************************/
int Joruus::CUser::nbChan ()
{
  return m_ChanList.size();
}

/***************************************************************************/
void Joruus::CUser::removeChan (string & chan)
{
  m_ChanList.erase(chan);
}

/***************************************************************************/
void Joruus::CUser::addChan (string & chan)
{
  setChanStatus (chan, '\0');
}

bool Joruus::CUser::isOnChannel (string & chan)
{
  return (m_ChanList.find(chan) != m_ChanList.end());
}

/***************************************************************************/
char Joruus::CUser::getChanStatus (string & chan)
{
  if (isOnChannel(chan))
  {
    return m_ChanList[chan];
  }
}
