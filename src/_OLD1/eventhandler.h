/***************************************************************************
 *            eventhandler.h
 *
 *  Mon Feb 12 17:36:25 2007
 *  Copyright  2007  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _EVENTHANDLER_H
#define _EVENTHANDLER_H

#include "globals.h"


class Joruus::CEventHandler 
{
  private:
    /* Function to be called */
    TEventHandler Handler;

  public:
    /* Constructor and destructor */
    CEventHandler (int (*newHdl) (TEvent *) = NULL);
    virtual ~CEventHandler ();
    
    /* Set the handler */
    void setHandler ( THandler );
    
    /* return the associated handler */
    THandler getHandler ();

    /* Run the associated code */
    int RunHandler (Joruus::TEvent * evt);
};

#endif /* _EVENTHANDLER_H */
