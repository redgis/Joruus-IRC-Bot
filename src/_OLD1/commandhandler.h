/***************************************************************************
 *            commandhandler.h
 *
 *  Thu Feb 15 11:20:39 2007
 *  Copyright  2007  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef _COMMANDHANDLER_H
#define _COMMANDHANDLER_H


#include "globals.h"


class Joruus::CCommandHandler 
{
  private:
    /* Function to be called */
    TCommandHandler m_Handler;

  public:
    /* Constructor and destructor */
    CCommandHandler (int (*newHdl) (string &) = NULL);
    virtual ~CCommandHandler ();
    
    /* Set the handler */
    void setHandler (TCommandHandler);
    
    /* return the associated handler */
    THandler getHandler ();

    /* Run the associated code */
    int RunHandler (string &);
};




#endif /* _COMMANDHANDLER_H */
