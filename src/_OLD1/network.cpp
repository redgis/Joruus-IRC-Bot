/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "network.hpp"



extern "C" {

#include <netinet/in.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*****************************************************************************/
/* resolution DNS : on veut l'IP en fonction du parametre en chaine de car.
 * (a nous de trouver si c'est une IP ou un DNS) */
unsigned long int GetIP (const char * Host)
{
  /* variables locales */
  unsigned long int AdrIp;
  struct in_addr * LAdr;
  struct hostent * HostInfo;
   

  /* On essaie de convertir le Host en adresse IP, si  retourne -1,
   * c'est que echec, c'est donc qu'il s'agit d'un nom de domaine */
  AdrIp = inet_addr (Host);
  
  if (AdrIp == INADDR_NONE)
  {
    /* On fait donc une resolution DNS */
    HostInfo = gethostbyname (Host);
    if (HostInfo == nullptr)
    {
      Joruus::sysMessagePrint(hstrerror (h_errno), (char *) __FILE__, __LINE__);
      AdrIp = -1;
    }
    else
    {
      /* Recuperation de l'IP */
      LAdr = (struct in_addr *) HostInfo->h_addr_list[0];
      AdrIp = LAdr->s_addr;
      //printf (" %s... %s... ", Host, inet_ntoa(*LAdr));
    }
  }

  //printf (" %u... ", AdrIp);
  return AdrIp;
}



/*****************************************************************************/
/* resolution DNS inverse : on veut le nom de domaine en fonction de l'IP */
char * GetDNS (unsigned long int AdrIp)
{
  struct in_addr LAdr;
  struct hostent * HostInfo;
  char strAdr[16];            /* xxx.xxx.xxx.xxx */
  static char HostName[256];
  
  LAdr.s_addr = AdrIp;
  strcpy (strAdr, (char *) inet_ntoa (LAdr));
  
  HostInfo = gethostbyaddr (strAdr, strlen(strAdr), AF_INET);
  if (HostInfo == nullptr)
  {
    Joruus::sysMessagePrint (hstrerror (h_errno), (char *) __FILE__, __LINE__);
    strcpy (HostName, "(dns error)");
  }
  /* Recuperation du nom */
  else
  {
    strcpy (HostName, HostInfo->h_name);
  }
  return HostName;
}

/*****************************************************************************/
/* TCP connect to a listening IP address / port. Returns the socket */
int TCP_Connect_to (unsigned long int AdrIP, unsigned short int Port)
{
  int Socket;
  int SizeLocalAdr, SizeRemoteAdr;
  struct sockaddr_in LocalAdr;
  struct sockaddr_in RemoteAdr;
  int Res;
  
  //printf (" %u... \n", AdrIP);

  /* initialisation les donnees pour l'adresse locale */
  LocalAdr.sin_family = AF_INET;
  LocalAdr.sin_port = htons (0);  /* en local : n'importe quel port ! */
  LocalAdr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  /* initialisation les donnees pour l'adresse serveur */
  RemoteAdr.sin_family = AF_INET;
  RemoteAdr.sin_port = htons (Port);
  RemoteAdr.sin_addr.s_addr = /*htonl(*/AdrIP/*)*/;
  
  SizeLocalAdr = sizeof (LocalAdr);
  SizeRemoteAdr = sizeof (RemoteAdr);
    
  /* Creation socket */
  Socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (Socket == -1)
    return -1;
    
  /* On associe la socket a l'adresse et au port local */
  Res = bind (Socket, (struct sockaddr *) &LocalAdr, sizeof(LocalAdr));
  if (Res == -1)
    return -1;
  
  /* On se connecte a l'adresse et au port distants */
  Res = connect (Socket, (struct sockaddr *) &RemoteAdr, sizeof(RemoteAdr));
  if (Res == -1)
    return -1;

  return Socket;
}



/*****************************************************************************/
/* User should not use this */
/* We create a local structure to be ready to create an UDP socket to be able to send UDP data. */
int UDP_Init_Socket ()
{
  int Sock;
  struct sockaddr_in LocalAdr;
  int Res;

  /* initialisation les donnees pour l'adresse locale */
  LocalAdr.sin_family = AF_INET;
  LocalAdr.sin_addr.s_addr = htonl(INADDR_ANY);
  LocalAdr.sin_port = htons(0);
  
  /* Creation socket */
  Sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(Sock == -1)
    return -1;
  
  /* On associe la socket au port */
  Res = bind (Sock, (struct sockaddr *) &LocalAdr, sizeof(LocalAdr));
  if (Res == -1)
    return -1;
  
  /* On passe la socket en mode non bloquant. idem pour l'entree clavier */
  fcntl (Sock, F_SETFL, O_NONBLOCK | fcntl (Sock, F_GETFL));
  
  return Sock;
}



/*****************************************************************************/
/* Ouvre la socket serveur en ecoute TCP */
int TCP_Open_Listening (unsigned short int Port, unsigned short int * PortOuvert = nullptr)
{
  struct sockaddr_in ServAdr;
  int SizeAdr;
  int ServerSock = -1;
  int Res;

  /* initialisation des donnees pour la connexion serveur */
  ServAdr.sin_family = AF_INET;
  ServAdr.sin_port = htons (Port);
  ServAdr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  SizeAdr = sizeof (ServAdr);

  /* creation de la socket */
  ServerSock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (ServerSock == -1)
  {
    Joruus::sysMessagePrint ((string ("Unable to open listening TCP socket") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    exit(0);
  }

  /* On lie la socket au port voulu, sur l'IP voulue */
  Res = bind (ServerSock, (const struct sockaddr*) &ServAdr, SizeAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint ((string("Unable to bind listening TCP socket on port: ") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    exit (0);
  }
  
  /* On met la socket en ecoute */
  listen (ServerSock, 5);
  
  /* On passe la socket en mode non bloquant. idem pour l'entree clavier */
  fcntl (ServerSock, F_SETFL, O_NONBLOCK | fcntl (ServerSock, F_GETFL));
  
  if (PortOuvert != nullptr)
  {
    (*PortOuvert) = ntohs(ServAdr.sin_port);
  }
  
  return ServerSock;
}

/*****************************************************************************/
/* Ouvre la socket serveur en ecoute UDP */
int UDP_Open_Listening (unsigned short int Port, unsigned short int * PortOuvert = nullptr)
{
  int SizeAdr;
  int ServerSock = -1;
  int Res;
  struct sockaddr_in ServAdr;
  
  /* initialisation des donnees pour la connexion serveur */
  ServAdr.sin_family = AF_INET;
  ServAdr.sin_port = htons (Port);
  ServAdr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  SizeAdr = sizeof (ServAdr);

  /* creation de la socket */
  ServerSock = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (ServerSock == -1)
  {
    Joruus::sysMessagePrint ((string ("Unable to open listening UDP socket") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    exit(0);
  }

  /* On lie la socket au port voulu, sur l'IP voulue */
  Res = bind (ServerSock, (const struct sockaddr*) &ServAdr, SizeAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint ((string("Unable to bind listening UDP socket on specified port: ") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    exit (0);
  }
  
  /* On met la socket en ecoute */
  listen (ServerSock, 5);
  
  /* On passe la socket en mode non bloquant. idem pour l'entree clavier */
  fcntl (ServerSock, F_SETFL, O_NONBLOCK | fcntl (ServerSock, F_GETFL));
  
  if (PortOuvert != nullptr)
  {
    (*PortOuvert) = ntohs(ServAdr.sin_port);
  }
  
  return ServerSock;
}



/*****************************************************************************/
int TCP_Send_Message (int Socket, const char * Message)
{
  int Size = strlen (Message);
  int Sent = 0, ToSend = Size;
  bool Exit = false;
  int Res = 0;
  
  while (!Exit)
  {
    Res = write (Socket, Message, ToSend);
    
    Sent += Res;
    
    ToSend = Size - Sent;
    if (Sent == Size)
      Exit = true;
    
    if (Res == -1)
      Exit = true;
  }
  
  return Res;
}
    
/*****************************************************************************/
int UDP_Send_Message (unsigned long int AdrIP, unsigned short int Port, const char * Message)
{
  int Size = strlen (Message);
  int Sent = 0, ToSend = Size;
  bool Exit;
  int Res = 0;
  int SocketUDP;
  struct sockaddr_in RemoteAdr;

  /* initialisation les donnees pour l'adresse serveur */
  RemoteAdr.sin_family = AF_INET;
  RemoteAdr.sin_port = htons (Port);
  RemoteAdr.sin_addr.s_addr = htonl(AdrIP);
  
  /* Initialise the socket */
  SocketUDP = UDP_Init_Socket ();


  if (SocketUDP != -1)
  {
    while (!Exit)
    {
    
      Res = sendto(SocketUDP, Message, Size, 0, (struct sockaddr *) &RemoteAdr, sizeof(RemoteAdr));
      
      Sent += Res;
      
      ToSend = Size - Sent;
      if (Sent == Size)
      {
        Exit = true;
        Res = Sent;
      }
      
      if (Res == -1)
        Exit = true;
    }

    close (SocketUDP);
  }
  
  return Res;
}

} /* extern "C" */
