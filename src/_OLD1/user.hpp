/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _USER_H
#define _USER_H

#include "../joruus.hpp"

class Joruus::CUser
{
  protected:
    CServer * m_Server; /* Server the user is connected to */
  
    string m_Nickname;      /* User nick name */
    string m_Name;      /* User name */
    string m_Domain;    /* User dns */
    string m_RealName;  /* User real name */
    
    time_t m_IdleTime;    /* Idle time in seconds */
    
    trilean m_IrcOp;    /* Is an IrcOp ? yes, no, unknown */
  
    map <string, char> m_ChanList;  /* User status for each Chan. We keep only the name of the chan because the user can be on channels that are not joined by me, and thus the corresponding CChannel doesn't exist */
  
  public:
    /* Constructor/destructor */
    CUser (CServer * server = nullptr, string nick = string("Joruus"),
           string name = string("JoruusCBot"), string domain = string("joruus.net"),
           string realname = string("Joruus C'Baoth, the Crazy Clone"));
    virtual ~CUser ();
  
    /* Accessors */
    void setServer (CServer *);
    void setNick (string &);
    void setName (string &);
    void setDomain (string &);
    void setRealName (string &);
    void setIdleTime (time_t );
    void setIrcOp (trilean);
    void setChanStatus (string & , char);
    void clearChans ();
    void addChan (string &);
    void removeChan (string &);
    
    CServer * getServer ();
    string & getNick ();
    string & getName ();
    string & getDomain ();
    string & getRealName ();
    time_t getIdleTime ();
    trilean getIrcOp ();
    map<string,char> & getChanList ();
    char getChanStatus (string &);
    bool isOnChannel (string &);

    // Methods
    int nbChan ();
};


#endif /* _USER_H */
