/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _THREAD_H_
#define _THREAD_H_

#include "../joruus.hpp"
#include <pthread.h>

/* Pour utiliser cette classe, il faut cr�er une classe d�riv�e 
  en �crivant le code de la m�thode execute() qui repr�sente le
  thread. Pour compiler la classe derivee, il faut taper:
  g++ -D_REENTRANT -o derivee derivee.cpp -lpthread
*/

class Joruus::CThread
{
  private:
    pthread_t leThread;
    int state;
  
  public:
    CThread()
    {
      state = 0;
    }

    virtual ~CThread()
    {
      if(state == 1)
        pthread_cancel(leThread);
      pthread_detach(leThread);
    }

    pthread_t* create()
    {
      if(!pthread_create(&(this->leThread), nullptr, CThread::start, static_cast<void *>(this)))
      {
        state = 1;
        return &(this->leThread);
      }
      else
      {
        sysMessagePrint("Failed to create thread!", (char *)__FILE__, __LINE__);
        exit(0);
      }
    }

    static void* start(void* thread)
    {
      static_cast<CThread *>(thread)->run();
      pthread_exit(0);
      return thread;
    }


    void join()
    {
      void *value = 0;
      pthread_join(leThread, &value);
    }

    void cancel()
    {
      if (state==1)
      {
        state=0;
        pthread_cancel(leThread);
      }
      pthread_detach(leThread);
    }

    virtual void run() = 0;
};

#endif /* _THREAD_H_ */

