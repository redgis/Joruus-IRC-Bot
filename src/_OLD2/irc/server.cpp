/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "server.hpp"
#include "user.hpp"
#include "channel.hpp"

/***************************************************************************/
Joruus::CServer::CServer (CClient_IRCConnexion * connection) : m_Connexion (connection)
{

}

/***************************************************************************/
Joruus::CServer::~CServer ()
{
}

/***************************************************************************/
string & Joruus::CServer::getHostname ()
{
  return m_Hostname;
}

/***************************************************************************/
CClient_IRCConnexion * Joruus::CServer::getConnection ()
{
  return m_Connexion;
}

/***************************************************************************/
void Joruus::CServer::addChannel (CChannel & channel)
{
  m_ChannelsByName[channel.getChanName ()] = channel;
}

/***************************************************************************/
bool Joruus::CServer::hasChannel (string & channame)
{
  return ( m_ChannelsByName.find(channame) != m_ChannelsByName.end() );
}

/***************************************************************************/
CChannel * Joruus::CServer::getChanByName (string & channame)
{
  CChannel & resChan = m_ChannelsByName.find(channame);

  if (resChan == m_ChannelsByName.end())
  {
    return nullptr;
  }
  else
  {
    return &resChan;
  }
}

/***************************************************************************/
void Joruus::CServer::removeChannel (CChannel & channel)
{
  m_ChannelsByName.erase(channel.getChanName ());
}

/***************************************************************************/
void Joruus::CServer::removeChannel (string & channame)
{
  CChannel & channel = m_ChannelsByName[channame];
  if (channel != m_ChannelsByName.end())
    removeChannel (channel);
}

/***************************************************************************/
void Joruus::CServer::addUser (CUser * user)
{
  m_UsersByName[user->getName()] = user;
}

/***************************************************************************/
void Joruus::CServer::addUser (CUser * user)
{
  m_UsersByName[user->getName()] = (*user);
}

/***************************************************************************/
CUser * Joruus::CServer::getUserByName (string & username)
{
  CUser & resUser = m_UsersByName.find(username);

  if (resUser == m_UsersByName.end())
  {
    return nullptr;
  }
  else
  {
    return &resUser;
  }
}

/***************************************************************************/
void Joruus::CServer::removeUser (CUser & user)
{
  m_UsersByName.erase(user.getName());
}

/***************************************************************************/
void Joruus::CServer::removeUser (string & username)
{
  CUser & user = m_UsersByName[username];
  if (user != m_UsersByName.end())
    removeUser (user);
}

/***************************************************************************/
bool Joruus::CServer::hasUser (string & username)
{
  return (m_UsersByName.find (username) != m_UsersByName.end());
}

/***************************************************************************/
bool Joruus::CServer::hasUser (CUser & user)
{
  return hasUser(user.getName());
}

/***************************************************************************/
void Joruus::CServer::UserJoinChannel (string & name, string & newChan)
{
  // find or create user
  CUser * tmpUser = getUserByName(name);
  if (tmpUser == nullptr)
  {
    tmpUser = new CUser(this);
  }
  addUser (tmpUser);

  // find or create chan
  CChannel * tmpChan = getChanByName(newChan);
  if (tmpChan == nullptr)
  {
    tmpChan = new CChannel (newChan, this);
  }
  addChannel ((*tmpChan));

  // add user to chan
  tmpChan->addUser(tmpUser);
}

/***************************************************************************/
void Joruus::CServer::UserPartChannel (string & name, string & prevChan)
{
  CChannel * tmpChan = getChanByName(prevChan);
  CUser * tmpUser = getUserByName(name);

  if (tmpUser != nullptr)
  {
    //if user is orphan
    if ((tmpUser->nbChan() == 0) && (tmpUser != m_Me))
    {
      removeUser(*tmpUser);

      //remove user from chan
      if (tmpChan != nullptr)
        tmpChan->removeUser(tmpUser);
    }
  }

  //remove chan if empty
  if (tmpChan->isEmpty())
    removeChannel (*tmpChan);
}

/***************************************************************************/
void Joruus::CServer::UserQuit (string & name)
{
  // remove user from all chans
    //if chan empty, remove it


  // if user is orphan, remove
}

/***************************************************************************/
void Joruus::CServer::UserChangeChannelStatus (string & name, string &chan, char status)
{

}

