/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _CHANNEL_H
#define _CHANNEL_H

#include "../joruus.hpp"

class Joruus::CChannel
{
  private:
    string m_Name;
    string m_Modes;
    string m_Topic;
    CServer * m_Server;

    /* Association of user class and chan mode with nickname */
    map <CUser *, char> m_UsersStatus;  /* User status for each Chan. We keep only the name of the chan because the user can be on channels that are not joined by me, and thus the corresponding CChannel doesn't exist */

  public:
    /* Constructors / destructors */
    CChannel (string & name, CServer *);
    virtual ~CChannel ();
    
    string & getChanName ();
    string & getModes ();
    string & getTopic ();
    CServer * getServer ();

    void setChanName (string &);
    void setModes (string &);
    void unsetModes (string &);
    void setTopic (string &);
    void setServer (CServer *);
    
    /* User managment */
    void addUser (CUser *);
    void setUserStatus (CUser *, char);
    bool hasUser (CUser *);
    void removeUser (CUser *);

    bool isEmpty ();
    int NbUsers ();
};

#endif /* _CHANNEL_H */
