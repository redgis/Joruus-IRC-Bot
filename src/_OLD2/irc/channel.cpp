/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "channel.hpp"
#include "server.hpp"
#include "user.hpp"

/***************************************************************************/
Joruus::CChannel::CChannel (string & Name, CServer * server) : m_Name(Name), m_Server(server)
{
}

/***************************************************************************/
Joruus::CChannel::~CChannel ()
{

}

/***************************************************************************/
string & Joruus::CChannel::getChanName ()
{
  return m_Name;
}

/***************************************************************************/
string & Joruus::CChannel::getModes ()
{
  return m_Modes;
}

/***************************************************************************/
string & Joruus::CChannel::getTopic ()
{
  return m_Topic;
}

/***************************************************************************/
CServer * Joruus::CChannel::getServer ()
{
  return m_Server;
}

/***************************************************************************/
void Joruus::CChannel::setChanName (string & name)
{
  m_Name = name;
}

/***************************************************************************/
void Joruus::CChannel::setModes (string & modes)
{
}

/***************************************************************************/
void Joruus::CChannel::unsetModes (string & modes)
{

}

/***************************************************************************/
void Joruus::CChannel::setTopic (string & top)
{
  m_Topic = top;
}

/***************************************************************************/
void Joruus::CChannel::setServer (CServer * server)
{
  m_Server = server;
}

/***************************************************************************/
void Joruus::CChannel::addUser (CUser * user)
{
  if (user != nullptr)
    m_UsersStatus[user] = "";
}

/***************************************************************************/
void Joruus::CChannel::setUserStatus (CUser * user, char c)
{
  if (user != nullptr)
    if (hasUser(user))
      m_UsersStatus[user] = c;
}

/***************************************************************************/
bool Joruus::CChannel::hasUser (CUser * user)
{
  if (user != nullptr)
    return (m_UsersStatus.find(user) != m_UsersStatus.end());
  else
    return false;
}

/***************************************************************************/
bool Joruus::CChannel::isEmpty ()
{
  return (m_UsersStatus.size) == 0);
}

/***************************************************************************/
int Joruus::CChannel::NbUsers ()
{
  return m_UsersStatus.size ();
}
