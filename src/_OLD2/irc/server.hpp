/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _SERVER_H
#define _SERVER_H

#include "../joruus.hpp"

class Joruus::CServer
{
  private:
    
    CClient_IRCConnexion * m_Connexion;        /* TCP connexion to the server */
    string m_Hostname;                          /* Server hostname */
    map <string, CChannel> m_ChannelsByName;  /* List of chans known on this server */
    map <string, CUser> m_UsersByName;        /* List of users known on this server */
    CUser m_Me;                               /* Me in this server */

  public:
    /* Constructors / destructors */
    CServer (CClient_IRCConnexion * connection);
    virtual ~CServer ();

    string & getHostname ();
    CClient_IRCConnexion * getConnection ();

    /* adds an association of a chan name to a CChannel class */
    void addChannel (string &);
    void addChannel (CChannel &);
    bool hasChannel (string &);
    CChannel * getChanByName (string &);
    void removeChannel (string &);
    void removeChannel (CChannel &);
    
    /* User managment */
    void addUser (CUser *);
    void addUser (string &);
    bool hasUser (string &);
    bool hasUser (CUser &);
    CUser * getUserByName (string &);
    void removeUser (CUser &);
    void removeUser (string &);


    //Management
    void UserJoinChannel (string & name, string & newChan);
    void UserPartChannel (string & name, string & prevChan);
    void UserQuit (string & name);
    void UserChangeChannelStatus (string & name, string &chan, char status);

};

#endif /* _SERVER_H */
