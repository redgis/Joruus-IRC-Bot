/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef IRCMESSAGE_HPP_
#define IRCMESSAGE_HPP_

#include "../joruus.hpp"

/****************************************************************************/
class Joruus::CIrcMessage
{
  public:
    CIrcMessage (string Data);
    virtual ~CIrcMessage();

    string m_Content;
};

#endif /* IRCMESSAGE_HPP_ */
