/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "user.hpp"


/***************************************************************************/
Joruus::CUser::CUser (CServer * server, string nick, string name, string domain, string realname) : m_Server(server),m_Nickname(nick),m_Name(name),m_Domain(domain),m_RealName(realname)
{
  m_IdleTime = -1;
  m_IrcOp = UNKNOWN;
  m_ChanList.clear ();
}

/***************************************************************************/
Joruus::CUser::~CUser ()
{
  
}
