/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _USER_H
#define _USER_H

#include "../joruus.hpp"

class Joruus::CUser
{
  public:
    /* Constructor/destructor */
    CUser (CServer * server = nullptr, string nick = string("Joruus"),
           string name = string("JoruusCBot"), string domain = string("joruus.net"),
           string realname = string("Joruus C'Baoth, the Crazy Clone"));

    virtual ~CUser ();

    CServer * m_Server; /* Server the user is connected to */
  
    string m_Nickname;      /* User nick name */
    string m_Name;      /* User name */
    string m_Domain;    /* User dns */
    string m_RealName;  /* User real name */
    time_t m_IdleTime;    /* Idle time in seconds */
    trilean m_IrcOp;    /* Is an IrcOp ? yes, no, unknown */
  
    time_t m_lastUpdate  /* When it has been last updated */
};


#endif /* _USER_H */
