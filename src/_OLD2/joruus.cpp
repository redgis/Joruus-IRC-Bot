/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "joruus.hpp"


Joruus::CBot * Joruus::JoruusCBot = nullptr;

/* Mappings from keyword to EventType / Command */
map <string, Joruus::CEventType *> Joruus::EventTypeMap;
map <string, Joruus::CCommand *> Joruus::CommandMap;


/****************************************************************************/
int Joruus::init ()
{
  int result = 0;

  Joruus::EventTypeMap.clear ();
  Joruus::CommandMap.clear ();

  //result = sem_init (&(sem_EventMgr), 0, 1);
  //if (result == -1)
  //  debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);

  return result;
}


/***************************************************************************/
int Joruus::parseArguments (char * szArguments, char * args[])
{
  int nbargs = 0;
  int linelength = strlen (szArguments);
  bool onText = false;

  if(linelength > 0)
  {
    
    for (int Index = 0; Index < linelength; Index ++)
    {
      if ( (szArguments[Index] != ' ')
        && (szArguments[Index] != '\n')
        && (szArguments[Index] != '\t')
        && (szArguments[Index] != '\r')
        && (szArguments[Index] != '\b')
        && (szArguments[Index] != '\0'))
      {
        // Last seen is a space, and we find some text : it is an argument
        if (!onText)
        {
          args[nbargs] = szArguments+Index;
          nbargs++;
          onText = true;
        }
      }
      else
      {
        // Last seen is somme text, and we find a space : end of the argument,
        // insert a nullptr terminating caracter
        if (onText)
        {
          szArguments[Index] = '\0';
          onText = false;
        }
      }
    }
  }

  return nbargs;
}


/****************************************************************************/
void Joruus::debugMessagePrint (const char * Message, char * inFile, int atLine, TTimestamp theTime)
{
# ifdef _DEBUG_
  struct tm * msgTime;
  char strTime[24];
  msgTime = localtime( (const time_t *) &theTime );
  
  //strftime(strTime, 20, "%Y-%I-%D %H:%M:%S", &msgTime );
  strftime(strTime, 20, "%H:%M:%S", msgTime );

  cout << CONSOLE_DGRAY << strTime << " -DEBUG- " << inFile << ":" << atLine << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
# endif

  return;
}

/****************************************************************************/
void Joruus::sysMessagePrint (const char * Message, char * inFile, int atLine, TTimestamp theTime)
{
  struct tm * msgTime;
  char strTime[24];
  msgTime = localtime((const time_t *) &theTime );
  
  //strftime(strTime, 20, "%Y-%I-%D %H:%M:%S", &msgTime );
  strftime(strTime, 20, "%H:%M:%S", msgTime );
  
# ifdef _DEBUG_
  cout << CONSOLE_GRAY << strTime << " -SYSTEM- " << inFile << ":" << atLine << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
# else
  cout << CONSOLE_GRAY << strTime << " -SYSTEM- " << Message << CONSOLE_RESET_COLOR << endl << flush;
#endif
}

/****************************************************************************/
void Joruus::ircStatusPrint (const char * Message, char * serverName, TTimestamp theTime)
{
  struct tm * msgTime;
  char strTime[24];
  msgTime = localtime((const time_t *) &theTime );
  
  //strftime(strTime, 20, "%Y-%I-%D %H:%M:%S", &msgTime );
  strftime(strTime, 20, "%H:%M:%S", msgTime );
  
  cout << CONSOLE_RED << strTime << " " << serverName << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
}

/****************************************************************************/
void Joruus::ircMessagePrint (const char * Message, char * serverName, char * from, char * to, TTimestamp theTime)
{
  if ( (serverName == nullptr) || (Message == nullptr))
    return;

  struct tm * msgTime;
  char strTime[24];
  msgTime = localtime( (const time_t *) &theTime );
  
  //strftime(strTime, 20, "%Y-%I-%D %H:%M:%S", &msgTime );
  strftime(strTime, 20, "%H:%M:%S", msgTime );

  if ((from == nullptr) || (to == nullptr) )
    cout << CONSOLE_GREEN << strTime << " " << serverName << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
  else
    cout << CONSOLE_WHITE << strTime << " " << serverName << " " << from << "@" << to << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
}

/****************************************************************************/
void Joruus::ircNoticePrint (const char * Message, char * serverName, char * from, char * to, TTimestamp theTime)
{
  if ( (from == nullptr) || (to == nullptr) || (serverName == nullptr) || (Message == nullptr))
    return;

  struct tm * msgTime;
  char strTime[24];
  msgTime = localtime( (const time_t *) &theTime );
  
  //strftime(strTime, 20, "%Y-%I-%D %H:%M:%S", &msgTime );
  strftime(strTime, 20, "%H:%M:%S", msgTime );

  cout << CONSOLE_MAGENTA << strTime << " " << serverName << " --" << from << "@" << to << " " << Message << CONSOLE_RESET_COLOR << endl << flush;
}

