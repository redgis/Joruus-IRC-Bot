/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "default_cmd_handlers.hpp"

namespace Joruus {

  // (string cmd, string desc = "", string use= "Not specified", int args = 0)

  /* defines of default commands : ( -1 = any) */
  int CommandNbArgs[][2] = {
    {1, 2},  //CONNECT
    {1, 1},  //JOIN
    {2, -1}, //MSG
    {1, 1},  //NICK 
    {1, -1}, //PART
    {1, 1},  //TALKTO
    {1, 1},  //USESERVER
    {0, 0}
  };

  const char * Command[][3] = {
    { "CONNECT", "Connect to specified server on specified port.", "/CONNECT {SERVER_ADDRESS} [PORT]\nExample: /connect irc.langochat.net 6667"},
    { "JOIN",    "Join one or more channel.", "/JOIN {CHANNEL}\nExample: /join #joruus"},
    { "MSG",     "Send a message to specified channel or user.",   "/MSG {CHANNEL|NICK} {MESSAGE}\nExample: /msg #programmation hello, world !"},
    { "NICK",    "Change your nickname.", "/NICK {NICKNAME}\nExample: /nick Jorus"},
    { "PART",    "Leave a channel.", "/PART {CHANNEL} [QUIT_MESSAGE]\nExample: /part #Joruus bye everybody !"},
    { "TALKTO",    "Select your current interlocutor, i.e. the channel or user your input is sent to.", "/TALKTO {NICKNAME|CHANNEL}\nExample: /talkto #joruus"},
    { "USESERV", "Select server you are talking to.", "/USESERV {SERVERID}\nExample: /useserv 2"},
    { "\0",      "\0",                                             "\0"}
  };

  /* defines of default commands : */
  TCommandHandler CommandDefaultHandler[] = {
    h_cmdCONNECT,
    h_cmdJOIN,
    h_cmdMSG,
    h_cmdNICK,
    h_cmdPART,
    h_cmdTALKTO,
    h_cmdUSESERVER,
    nullptr
  };
}


/****************************************************************************/
/* Definitions of default handlers */


/****************************************************************************/
int Joruus::h_cmdCONNECT (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line

  char * host;
  char * port = (char *) "6667\0";
    
  int Res;

  if (argc >= 1)
    host = argv[0];
  if (argc == 2)
    port = argv[1];
  
  Joruus::CIRCServerConnection * newConnection = new Joruus::CIRCServerConnection (host, atoi ((const char *) port), JoruusCBot);
  
  /* Connect to server */  
  Joruus::sysMessagePrint ((string ("Connecting to ") + host + ":" + port).c_str (), (char *) __FILE__, __LINE__);

  Res = newConnection->Connect();

  if (Res == -1)
    Joruus::sysMessagePrint ((string("Unable to connect to ") + host + ":" + port).c_str (), (char *) __FILE__, __LINE__);

  return Res;
}

/****************************************************************************/
int Joruus::h_cmdJOIN (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line  
  string Message = string("JOIN ")+argv[0];
  Joruus::JoruusCBot->getCurrentServer ()->IrcSendMessage (Message);
  Joruus::ircMessagePrint ((string ("Joining ")+argv[0]+"...").c_str(), (char *) Joruus::JoruusCBot->getCurrentServer()->m_Server->getHostname().c_str(), nullptr, nullptr);
  return 0;
}

/****************************************************************************/
int Joruus::h_cmdMSG (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line  
  string Message = string("PRIVMSG ")+argv[0]+" :"+Arguments.substr(argv[1]-argv[0]);
  Joruus::JoruusCBot->getCurrentServer ()->IrcSendMessage (Message);
  Joruus::ircMessagePrint (Arguments.substr(argv[1]-argv[0]).c_str(), (char *) Joruus::JoruusCBot->getCurrentServer ()->m_Server->getHostname ().c_str(), (char *) Joruus::JoruusCBot->getNick ().c_str(), argv[0]);
  return 0;
}

/****************************************************************************/
int Joruus::h_cmdNICK (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line
  if (Joruus::JoruusCBot->getCurrentServer() != nullptr)
  {
    string Message = string("NICK ")+argv[0];
    Joruus::JoruusCBot->getCurrentServer ()->IrcSendMessage (Message);
  }

  return 0;
}

/****************************************************************************/
int Joruus::h_cmdPART (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line  
  string Message = string("PART ")+argv[0];
  if (argc > 1)
    Message += " :"+Arguments.substr(argv[1]-argv[0]);

  Joruus::JoruusCBot->getCurrentServer ()->IrcSendMessage (Message);
  Joruus::ircMessagePrint ((string ("Leaving ")+argv[0]+"...").c_str(), (char *) Joruus::JoruusCBot->getCurrentServer()->m_Server->getHostname ().c_str(), nullptr, nullptr);

  return 0;
}

/****************************************************************************/
int Joruus::h_cmdTALKTO (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line
  if (Joruus::JoruusCBot->getCurrentServer() != nullptr)
    Joruus::JoruusCBot->getCurrentServer()->m_Interlocutor.assign((const char*) argv[0]);
  
  return 0;
}

/****************************************************************************/
int Joruus::h_cmdUSESERVER (string & Arguments, CCommand * Command, int argc, char * argv[])
{
  // Execute command line
  Joruus::JoruusCBot->setCurrentServer (atoi (argv[0]));
  return 0;
}
