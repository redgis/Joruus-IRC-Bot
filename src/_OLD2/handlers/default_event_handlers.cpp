/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "default_event_handlers.hpp"
#include <argz.h>

namespace Joruus {

  /* defines of default events : */
  const char * EventTypeKeywordsAndDescription[][2] = {
    { "UNKNOWN", ""},
    { "PASS", ""},
    { "NICK", ""},
    { "USER",  "" },
    { "SERVER", "" },
    { "QUIT", "" },
    { "SQUIT", "" },
    { "PING", "" },
    { "PONG", "" },
    { "OPER", "" },
    { "KILL", "" },
    { "ERROR", "" },
    { "JOIN", "" },
    { "PART", "" },
    { "MODE", "" },
    { "TOPIC", "" },
    { "NAMES", "" },
    { "LIST", "" },
    { "INVITE", "" },
    { "KICK", "" },
    { "WHOIS", "" },
    { "WHOWAS", "" },
    { "NOTICE", "" },
    { "PRIVMSG", "" },

//Replies
    { "221", "RPL_UMODEIS" },
    { "311", "RPL_WHOISUSER" },
    { "312", "RPL_WHOISSERVER" },
    { "313", "RPL_WHOISOPERATOR" },
    { "314", "RPL_WHOWASUSER" },
    { "317", "RPL_WHOISIDLE" },
    { "318", "RPL_ENDOFWHOIS" },
    { "319", "RPL_WHOISCHANNELS" },
    { "322", "RPL_LIST" },
    { "323", "RPL_LISTEND" },
    { "324", "RPL_CHANNELMODEIS" },
    { "331", "RPL_NOTOPIC" },
    { "332", "RPL_TOPIC" },
    { "341", "RPL_INVITING" },
    { "353", "RPL_NAMREPLY" },
    { "366", "RPL_ENDOFNAMES" },
    { "367", "RPL_BANLIST" },
    { "368", "RPL_ENDOFBANLIST" },
    { "369", "RPL_ENDOFWHOWAS" },
    { "381", "RPL_YOUREOPER" },
//Errors
    { "401", "ERR_NOSUCHNICK" },
    { "402", "ERR_NOSUCHSERVER" },
    { "403", "ERR_NOSUCHCHANNEL" },
    { "404", "ERR_CANNOTSENDTOCHAN" },
    { "406", "ERR_WASNOSUCHNICK" },
    { "409", "ERR_NOORIGIN" },
    { "411", "ERR_NORECIPIENT" },
    { "412", "ERR_NOTEXTTOSEND" },
    { "413", "ERR_NOTOPLEVEL" },
    { "414", "ERR_WILDTOPLEVEL" },
    { "431", "ERR_NONICKNAMEGIVEN" },
    { "432", "ERR_ERRONEUSNICKNAME" },
    { "433", "ERR_NICKNAMEINUSE" },
    { "436", "ERR_NICKCOLLISION" },
    { "442", "ERR_NOTONCHANNEL" },
    { "443", "ERR_USERONCHANNEL" },
    { "461", "ERR_NEEDMOREPARAMS" },
    { "462", "ERR_ALREADYREGISTRED" },
    { "464", "ERR_PASSWDMISMATCH" },
    { "467", "ERR_KEYSET" },
    { "471", "ERR_CHANNELISFULL" },
    { "472", "ERR_UNKNOWNMODE" },
    { "473", "ERR_INVITEONLYCHAN" },
    { "474", "ERR_BANNEDFROMCHAN" },
    { "475", "ERR_BADCHANNELKEY" },
    { "481", "ERR_NOPRIVILEGES" },
    { "482", "ERR_CHANOPRIVSNEEDED" },
    { "483", "ERR_CANNTKILLSERVER" },
    { "491", "ERR_NOOPERHOST" },
    { "501", "ERR_UMODUUNKNOWNFLAG" },
    { "502", "ERR_USERSDONTMATCH" },

    { "CONSOLE_MSG", "Console activity" },
    { "UNKNOWN_CMD", "Unkown console command"},
    { "\0", "\0" }
  };

  /* defines of default events : */
  TEventHandler EventTypeDefaultHandler[] = {
  
  //Basics
    h_UNKNOWN,
    h_PASS,
    h_NICK,
    h_USER,
    h_SERVER,
    h_QUIT,
    h_SQUIT,
    h_PING,
    h_PONG,
    h_OPER,
    h_KILL,
    h_ERROR,
    h_JOIN,
    h_PART,
    h_MODE,
    h_TOPIC,
    h_NAMES,
    h_LIST,
    h_INVITE,
    h_KICK,
    h_WHOIS,
    h_WHOWAS,
    h_NOTICE,
    h_PRIVMSG,
  
  //Replies
    h_221,
    h_311,
    h_312,
    h_313,
    h_314,
    h_317,
    h_318,
    h_319,
    h_322,
    h_323,
    h_324,
    h_331,
    h_332,
    h_341,
    h_353,
    h_366,
    h_367,
    h_368,
    h_369,
    h_381,
  
  //Errors
    h_401,
    h_402,
    h_403,
    h_404,
    h_406,
    h_409,
    h_411,
    h_412,
    h_413,
    h_414,
    h_431,
    h_432,
    h_433,
    h_436,
    h_442,
    h_443,
    h_461,
    h_462,
    h_464,
    h_467,
    h_471,
    h_472,
    h_473,
    h_474,
    h_475,
    h_481,
    h_482,
    h_483,
    h_491,
    h_501,
    h_502,
  
  //Others
    h_CONSOLE_MSG,
    h_UNKNOWN_CMD,
    nullptr
  };
}


/****************************************************************************/
/* Definitions of default handlers */


int Joruus::h_UNKNOWN (Joruus::TEvent* evt)
{
  Joruus::sysMessagePrint((string("Unrecognized: ")+evt->Keyword+" "+evt->Data).c_str(), (char *) __FILE__, __LINE__, evt->Timestamp);
  return 0;
}


int Joruus::h_PASS (Joruus::TEvent* evt)
{
  return 0;
  /*
  Message PASS

  Param�tre : <le mot de passe>

  R�ponses possibles : ERR_NEEDMOREPARAMS (461), ERR_ALREADYREGISTRED (462)

  Ce message est optionnel, certains r�seaux exigent que toutes les connexions soient identifi�es par un mot
  de passe, chaque serveur contenant une liste de mots de passe valides pour les clients et une pour les
  serveurs. Il est tr�s vivement recommand� que ce soit mis en place pour les connexions de serveurs. Ce
  message doit �tre le premier envoy�, il peut �tre envoy� plusieurs fois, seul le dernier est alors pris en
  compte. Il est invalide s'il est envoy� dans les autres cas.
  */
}

int Joruus::h_NICK (Joruus::TEvent* evt)
{
  string newNick = evt->Data.substr(evt->Data.find_first_of(":")+1, evt->Data.find_first_of(IRC_SEPARATORS));

  Joruus::ircMessagePrint ((string (evt->Nick) + " is now known as " + newNick).c_str (), (char *) evt->Server->getHostname ().c_str (), nullptr, nullptr );
  
  /************* Update database *************/
  if (evt->User != nullptr)
    evt->User->setNick (newNick);
  
  return 0;
  /*
  Message NICK

  Param�tres : <nouveau NICK> [<Compteur>]

  R�ponses possibles : ERR_NONICKNAMEGIVEN (431), ERR_ERRONEUSNICKNAME (432),
  ERR_NICKNAMEINUSE (433), ERR_NICKCOLLISION (436)

  Ce message fait partie de la s�quence d'initialisation de connexion client/serveur, il permet � un client de
  sp�cifier le nick sous lequel il sera identifi�. Il permet �galement � un client de changer de nick une fois la
  connexion initialis�e, et peut donc �tre envoy� n'importe quand. Le compteur permet simplement d'indiquer �
  un serveur combien de serveur il y a entre le serveur qui lui a fait passer l'info et le client. Si un client fournit
  un compteur, alors il doit �tre ignor�. Si un serveur re�oit une information de changement de nick (de la part
  d'un autre serveur) et que le nouveau nick est d�j� enregistr�, alors il doit im�diatement aretter de
  transmettre l'info et envoyer une commande KILL ainsi qu'un ERR_NICKCOLLISION � l'attention du client
  pour d�connecter les deux clients. Si un client tente de changer de nick pour un nick d�j� utilis�, le serveur
  auquel il est directement connect� peut g�n�rer un ERR_NICKNAMEINUSE et �viter ainsi l'envoi de KILL (et
  donc les d�connexions).
  */
}

int Joruus::h_USER (Joruus::TEvent* evt)
{
  
  return 0;
}

int Joruus::h_SERVER (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_QUIT (Joruus::TEvent* evt)
{

  return 0;
}

int Joruus::h_SQUIT (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_PING (Joruus::TEvent* evt)
{
  Joruus::ircStatusPrint ((evt->Keyword+" "+evt->Data).c_str (),(char *)  (evt->Server->getHostname ().c_str ()), evt->Timestamp);
  string Message = "PONG " + evt->Data;
  evt->Server->getConnection()->IrcSendMessage (Message);
  Joruus::ircStatusPrint (Message.c_str(),(char *)  (evt->Server->getHostname ().c_str ())); 
  return 0;
  /*
  Messages PING/PONG

  Param�tres : [<param1> [<param2>]]

  R�ponses possibles : ERR_NOORIGIN (409), ERR_NOSUCHSERVER (402)

  Le classique PING/PONG, il permet de tester la validit� d'une connection, tout client recevant un message
  PING doit renvoyer un message PONG avec les m�mes arguments, sous peine de voir sa connection
  �ventuellement coup�e.
  */
}

int Joruus::h_PONG (Joruus::TEvent* evt)
{
  Joruus::ircStatusPrint ((evt->Keyword+" "+evt->Data).c_str (),(char *)  (evt->Server->getHostname ().c_str ()), evt->Timestamp);
  string Message = "PONG " + evt->Data;
  return 0;
  /*
  Messages PING/PONG

  Param�tres : [<param1> [<param2>]]

  R�ponses possibles : ERR_NOORIGIN (409), ERR_NOSUCHSERVER (402)

  Le classique PING/PONG, il permet de tester la validit� d'une connection, tout client recevant un message
  PING doit renvoyer un message PONG avec les m�mes arguments, sous peine de voir sa connection
  �ventuellement coup�e.
  */
}

int Joruus::h_OPER (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_KILL (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_ERROR (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_JOIN (Joruus::TEvent* evt)
{
  char * argv[256];
  char szArguments[evt->Data.size()+1];
  int nbargs;
  
  // Get arguments into char *
  strcpy (szArguments, evt->Data.c_str());
  nbargs = Joruus::parseArguments (szArguments, argv);
  
  CChannel * channel;
  string ChanName = argv[0];

  // If user doesn't exist, create it
  if (evt->User == nullptr)
    evt->User = new CUser (evt->Server, evt->Nick, evt->UserName, evt->Domain, string(""));
  
  // If channel doesn't exist, create it
  if (evt->Server->hasChannel(ChanName))
    channel = evt->Server->getChanByName(ChanName);
  else
  {
    channel = new CChannel (ChanName, evt->Server);
    evt->Server->addChannel (channel);
  }
  
  // Update users list of the chan : add this user to the channel
  channel->addUser (evt->User);
    

  if (evt->Data[0] == ':')
    Joruus::ircMessagePrint ((evt->Nick+" has joined "+evt->Data.substr(1)).c_str(), (char *) evt->Server->getHostname().c_str(), nullptr, nullptr);
  else
    Joruus::ircMessagePrint ((evt->Nick+" has joined "+evt->Data).c_str(), (char *) evt->Server->getHostname().c_str(), nullptr, nullptr);
  return 0;

  /*
  Message JOIN

  Param�tres : <nom du chan> [<cl�>]

  R�ponses possibles : ERR_NEEDMOREPARAMS (461), ERR_BANNEDFROMCHAN (474),
  ERR_INVITEONLYCHAN (473), ERR_BADCHANNELKEY (475), ERR_CHANNELISFULL (471),
  ERR_NOSUCHCHANNEL (403), RPL_TOPIC (332)

  Ce message permet d'entrer sur un chan. Une fois qu'un utilisateur est sur un chan, il re�oit tous les
  messages (discussion et gestion) concernant ce chan. Si l'entr�e est valid�e, le serveur transmet le message
  aux autres serveurs et autres utilisateurs pr�sents sur le chan, puis le message RPL_TOPIC rappelle le topic
  du chan, et est suivi de messages RPL_NAMREPLY (353) avec la liste des nicks pr�sents sur le chan, et
  enfin par un RPL_ENDOFNAMES (366).
  */
}

int Joruus::h_PART (Joruus::TEvent* evt)
{
  char * argv[256];
  char szArguments[evt->Data.size()+1];
  int nbargs;
  int shift = 0;
  
  // Get arguments into char *
  strcpy (szArguments, evt->Data.c_str());
  nbargs = Joruus::parseArguments (szArguments, argv);
  
  /************* Display correct message *************/  
  if (nbargs > 1)
  {
    shift = ((*(argv[1]))==':'); //To skip the leading ':'
    Joruus::ircMessagePrint ((evt->Nick+" has left "+argv[0]+" ("+evt->Data.substr(argv[1]-argv[0]+shift)+")").c_str(), (char *) evt->Server->getHostname().c_str(), nullptr, nullptr);
  }
  else
  {
    Joruus::ircMessagePrint ((evt->Nick+" has left "+argv[0]).c_str(), (char *) evt->Server->getHostname().c_str(), nullptr, nullptr);
  }
  
  /************* Update database *************/
  string ChanName = argv[0];
  CChannel * channel = evt->Server->getChanByName (ChanName);
  
  // If user doesn't exist, create it
  if (evt->User == nullptr)
    evt->User = new CUser (evt->Server, evt->Nick, evt->UserName, evt->Domain, string(""));

  // If channel doesn't exist, create it
  if (evt->Server->hasChannel(ChanName))
    channel = evt->Server->getChanByName(ChanName);
  else
  {
    channel = new CChannel (ChanName, evt->Server);
    evt->Server->addChannel (channel);
  }
  
  // Update channel user list
  channel->removeUser (evt->User);
  // Update channel list of the user
  evt->User->removeChan (ChanName);
  // Delete user if it has no more channel
  if (evt->User->nbChan () == 0)
    evt->Server->removeUser (evt->User);
  
  if (evt->User == (CUser *) Joruus::JoruusCBot)
    evt->Server->removeChannel (ChanName);

  return 0;
  /*
  Message PART

  Param�tres : <nom du chan> [<commentaire>]

  R�ponses possibles : ERR_NEEDMOREPARAMS (461), ERR_NOSUCHCHANNEL (403),
  ERR_NOTONCHANNEL (442)

  Ce message permet de sortir d'un chan.
  */
}

int Joruus::h_MODE (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_TOPIC (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_NAMES (Joruus::TEvent* evt)
{
  
  return 0;
  /*
  Message NAMES

  Param�tres : <nom du chan>

  R�ponses possibles : RPL_NAMREPLY (353), RPL_ENDOFNAMES (366)

  Permet de redemander la liste des personnes pr�sentes sur le chan et leur status (op, voice, simple
  membre).
  */
}

int Joruus::h_LIST (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_INVITE (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_KICK (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_WHOIS (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_WHOWAS (Joruus::TEvent* evt)
{
  return 0;
}

int Joruus::h_NOTICE (Joruus::TEvent* evt)
{
  char * argv[256];
  char szArguments[evt->Data.size()+1];
  int nbargs;
  int shift = 0;

  // Get arguments into char *
  strcpy (szArguments, evt->Data.c_str());
  nbargs = Joruus::parseArguments (szArguments, argv);
  shift = ((*(argv[1]))==':'); //To skip the leading ':'

  if (evt->Nick == "")
    Joruus::ircNoticePrint (evt->Data.substr(argv[1]-argv[0]+shift).c_str(), (char *) evt->Server->getHostname().c_str(), (char *) evt->Domain.c_str(), argv[0], evt->Timestamp);
  else
    Joruus::ircNoticePrint (evt->Data.substr(argv[1]-argv[0]+shift).c_str(), (char *) evt->Server->getHostname().c_str(), (char *) evt->Nick.c_str(), argv[0], evt->Timestamp);
  return 0;
  /*
  Elles sont au nombre de 2 (NOTICE et PRIVMSG) et suivent la m�me syntaxe : 

  COMMANDE <nick ou chan> <message>

  La NOTICE se distingue du message classique (PRIVMSG) par une simple signification s�mantique : il n'y a pas
  de r�elle r�ponse attendue, c'est uniquement un envoie d'information. De plus, auncune r�ponse d'erreur (r�ponse
  num�rique)   ne   peut   �tre   envoy�   �   une   notice.   
  
  Pour   les   PRIVMSG,   les   r�ponses   possibles   sont   :
  ERR_NORECIPIENT      (411),      ERR_NOTEXTTOSEND      (412),      ERR_CANNOTSENDTOCHAN      (404),
  ERR_NOTOPLEVEL (413), ERR_WILDTOPLEVEL (414), ERR_NOSUCHNICK (401)
  */
}

int Joruus::h_PRIVMSG (Joruus::TEvent* evt)
{
  char * argv[256];
  char szArguments[evt->Data.size()+1];
  int nbargs;
  int shift = 0;

  // Get arguments into char *
  strcpy (szArguments, evt->Data.c_str());
  nbargs = Joruus::parseArguments (szArguments, argv);
  shift = ((*(argv[1]))==':'); //To skip the leading ':'

  if (evt->Nick == "")
    Joruus::ircMessagePrint (evt->Data.substr(argv[1]-argv[0]+shift).c_str(), (char *) evt->Server->getHostname().c_str(), (char *) evt->Domain.c_str(), argv[0], evt->Timestamp);
  else
    Joruus::ircMessagePrint (evt->Data.substr(argv[1]-argv[0]+shift).c_str(), (char *) evt->Server->getHostname().c_str(), (char *) evt->Nick.c_str(), argv[0], evt->Timestamp);
  return 0;
  /*
  Elles sont au nombre de 2 (NOTICE et PRIVMSG) et suivent la m�me syntaxe : 

  COMMANDE <nick ou chan> <message>

  La NOTICE se distingue du message classique (PRIVMSG) par une simple signification s�mantique : il n'y a pas
  de r�elle r�ponse attendue, c'est uniquement un envoie d'information. De plus, auncune r�ponse d'erreur (r�ponse
  num�rique)   ne   peut   �tre   envoy�   �   une   notice.   
  
  Pour   les   PRIVMSG,   les   r�ponses   possibles   sont   :
  ERR_NORECIPIENT      (411),      ERR_NOTEXTTOSEND      (412),      ERR_CANNOTSENDTOCHAN      (404),
  ERR_NOTOPLEVEL (413), ERR_WILDTOPLEVEL (414), ERR_NOSUCHNICK (401)
  */

}


int Joruus::h_221 (Joruus::TEvent* evt)
{
  /* RPL_UMODEIS
  Param�tres : <modes utilisateurs>
  R�ponse � : MODE
  Cette r�ponse indique les modes possibles pour les utilisateurs. */

  return 0;
}

int Joruus::h_311 (Joruus::TEvent* evt)
{
  /* RPL_WHOISUSER
  Param�tres : <nick> <nom> <domaine> <vrai nom>
  R�ponse � : WHOIS
  Cette r�ponse indique les informations disponibles pour le nick indiqu� dans le message WHOIS. */

  return 0;
}

int Joruus::h_312 (Joruus::TEvent* evt)
{
  /* RPL_WHOISSERVER
  Param�tres : <nick> <serveur> [<infos serveur>]
  R�ponse � : WHOIS, WHOWAS
  Cette r�ponse indique le serveur auquel est ou �tait connect� l'utilisateur. */

  return 0;
}

int Joruus::h_313 (Joruus::TEvent* evt)
{
  /* RPL_WHOISOPERATOR
  Param�tres : <nick>
  R�ponse � : WHOIS
  Cette r�ponse indique que l'utilisateur est un IRCOP. */

  return 0;
}

int Joruus::h_314 (Joruus::TEvent* evt)
{
  /* RPL_WHOWASUSER
  Param�tres : <nick> <nom> <domaine> <vrai nom>
  R�ponse � : WHOWAS
  Cette r�ponse indique les (anciennes) informations disponibles pour le nick indiqu� dans le message
  WHOWAS. */

  return 0;
}

int Joruus::h_317 (Joruus::TEvent* evt)
{
  /* RPL_WHOISIDLE
  Param�tres : <nick> <temps d'idle en secondes>
  R�ponse � : WHOIS
  Cette r�ponse indique le temps d'idle en secondes (temps depuis l'envoie du dernier message au serveur) de
  l'utilisateur. */

  return 0;
}

int Joruus::h_318 (Joruus::TEvent* evt)
{
  /* RPL_ENDOFWHOIS
  Param�tres : <nick>
  R�ponse � : WHOIS
  Cette r�ponse indique qu'on a envoy� toutes les informations sur l'utilisateur. */

  return 0;
}

int Joruus::h_319 (Joruus::TEvent* evt)
{
  /* RPL_WHOISCHANNELS
  Param�tres : <nick> <liste de channels avec status>
  R�ponse � : WHOIS
  Cette r�ponse indique les chans o� est pr�sent l'utilisateur ainsi que son status sur ceux-ci (@ pour op, +
  pour voice), le symbole du status pr�c�de le nom du chan o� il s'applique. */

  return 0;
}

int Joruus::h_322 (Joruus::TEvent* evt)
{
  /* RPL_LIST
  Param�tres : <nom du chan> <nombre d'utilisateurs pr�ssents> <topic>
  R�ponse � : LIST
  Cette r�ponse indique le d�but de la liste des chans enregistr�s sur le r�seau. */

  return 0;
}

int Joruus::h_323 (Joruus::TEvent* evt)
{
  /* RPL_LISTEND
  Param�tres : <>
  R�ponses � : LIST
  Cette r�ponse indique la fin de la liste des chans enregistr�s sur le r�seau. */

  return 0;
}

int Joruus::h_324 (Joruus::TEvent* evt)
{
  /* RPL_CHANNELMODEIS
  Param�tres : <modes chan>
  R�ponse � : MODE
  Cette r�ponse indique les modes possibles pour les chans. */

  return 0;
}

int Joruus::h_331 (Joruus::TEvent* evt)
{
  /* RPL_NOTOPIC
  Param�tres : <chan>
  R�ponse � : TOPIC
  Cette r�ponse indique que le chan concern� n'a pas de topic. */

  return 0;
}

int Joruus::h_332 (Joruus::TEvent* evt)
{
  /* RPL_TOPIC
  Param�tres : <chan> <topic>
  R�ponse � : JOIN, TOPIC
  Cette r�ponse indique le topic du chan concern�. */

  return 0;
}

int Joruus::h_341 (Joruus::TEvent* evt)
{
  /* RPL_INVITING
  Param�tres : <chan> <nick>
  R�ponse � : INVITE
  Cette r�ponse confirme l'invitation d'un utilisateur sur un chan. */

  return 0;
}

int Joruus::h_353 (Joruus::TEvent* evt)
{
  /* RPL_NAMREPLY
  Param�tres : <chan> <liste des utilisateurs avec status>
  R�ponse � : NAMES
  Cette r�ponse indique les utilisateurs pr�sents sur le chan ainsi que leur status, indiqu� de la m�me fa�on
  que pour RPL_WHOISCHANNELS (319). */

  return 0;
}

int Joruus::h_366 (Joruus::TEvent* evt)
{
  /* RPL_ENDOFNAMES
  Param�tres : <chan>
  R�ponse � : NAMES
  Cette r�ponse indique que tous les utilisateurs pr�sents sur le chan ont �t� indiqu�s. */

  return 0;
}

int Joruus::h_367 (Joruus::TEvent* evt)
{
  /* RPL_BANLIST
  Param�tres : <chan> <masque d'utilisateur banni>
  R�ponse � : MODE
  Cette r�ponse indique un masque d'utilisateur qui est banni du chan. */

  return 0;
}

int Joruus::h_368 (Joruus::TEvent* evt)
{
  /* RPL_ENDOFBANLIST
  Param�tres : <chan>
  R�ponse � : MODE
  Cette r�ponse indique que tous les masques d'utilisateurs bannis du chan ont �t� indiqu�s. */

  return 0;
}

int Joruus::h_369 (Joruus::TEvent* evt)
{
  /* RPL_ENDOFWHOWAS
  Param�tres : <nick>
  R�ponse � : WHOWAS
  Cette r�ponse indique que toutes anciennes informations concernants le nick ont �t� indiqu�es. */

  return 0;
}

int Joruus::h_381 (Joruus::TEvent* evt)
{
  /* RPL_YOUREOPER
  Param�tres : <>
  R�ponse � : OPER
  Cette r�ponse indique que le status d'IRCOP vous a bien �t� attribu�. */

  return 0;
}

int Joruus::h_401 (Joruus::TEvent* evt)
{
  /* ERR_NOSUCHNICK //Errors
  Param�tres : <nick>
  R�ponse � : KILL, MODE, INVITE, PRIVMSG
  Cette erreur est renvoy�e quand le nick n'a pas �t� trouv� sur le r�seau. */

  return 0;
}

int Joruus::h_402 (Joruus::TEvent* evt)
{
  /* ERR_NOSUCHSERVER
  Param�tres : <serveur>
  R�ponse � : SQUIT, PING, PONG
  Cette erreur est renvoy�e quand le serveur n'a pas �t� trouv� sur le r�seau. */

  return 0;
}

int Joruus::h_403 (Joruus::TEvent* evt)
{
  /* ERR_NOSUCHCHANNEL
  Param�tres : <chan>
  R�ponse � : JOIN, MODE, PART, KICK
  Cette erreur est renvoy�e quand le chan n'a pas �t� trouv� sur le r�seau. */

  return 0;
}

int Joruus::h_404 (Joruus::TEvent* evt)
{
  /* ERR_CANNOTSENDTOCHAN
  Param�tres : <chan>
  R�ponse � : PRIVMSG
  Cette erreur est renvoy�e quand vous ne pouvez envoyer de message au chan du fait des restrictions en
  cours sur ce chan. */

  return 0;
}

int Joruus::h_406 (Joruus::TEvent* evt)
{
  /* ERR_WASNOSUCHNICK
  Param�tres : <nick>
  R�ponse � : WHOWAS
  Cette erreur est renvoy�e quand aucune information (ancienne) sur le nick n'a �t� trouv�e sur le r�seau. */

  return 0;
}

int Joruus::h_409 (Joruus::TEvent* evt)
{
  /* ERR_NOORIGIN
  Param�tres : <>
  R�ponse � : PING, PONG
  Cette erreur est renvoy�e quand il n'y a pas eu d'origine (de param�tre) envoy�e avec cette commande. */

  return 0;
}

int Joruus::h_411 (Joruus::TEvent* evt)
{
  /* ERR_NORECIPIENT
  Param�tres : <>
  R�ponse � : PRIVMSG
  Cette erreur est renvoy�e quand vous n'avez pas sp�cifi� de destinataire pour un message. */

  return 0;
}

int Joruus::h_412 (Joruus::TEvent* evt)
{
  /* ERR_NOTEXTTOSEND
  Param�tres : <>
  R�ponse � : PRIVMSG
  Cette erreur est renvoy�e quand vous n'avez pas sp�cifi� de texte � envoyer pour votre message. */

  return 0;
}

int Joruus::h_413 (Joruus::TEvent* evt)
{
  /* ERR_NOTOPLEVEL
  Param�tres : <masque d'utilisateur>
  R�ponse � : PRIVMSG
  Cette erreur est renvoy�e quand vous ne sp�cifiez pas de domaine dans un masque de destinataire pour un
  message. */

  return 0;
}

int Joruus::h_414 (Joruus::TEvent* evt)
{
  /* ERR_WILDTOPLEVEL
  Param�tres : <masque>
  R�ponse � : PRIVMSG
  Cette erreur est renvoy�e quand vous avez sp�cifi� une �toile (i.e. un joker) comme dernier caract�re d'un
  masque destinataire pour un message. */

  return 0;
}

int Joruus::h_431 (Joruus::TEvent* evt)
{
  /* ERR_NONICKNAMEGIVEN
  Param�tres : <nick>
  R�ponse � : NICK, WHOIS, WHOWAS
  Cette erreur est renvoy�e quand vous n'avez pas indiqu� de nick alors qu'il y en avait un d'attendu. */

  return 0;
}

int Joruus::h_432 (Joruus::TEvent* evt)
{
  /* ERR_ERRONEUSNICKNAME
  Param�tres : <nick>
  R�ponse � : NICK
  Cette erreur est renvoy�e quand le nick contient des caract�res non autoris�s par le r�seau. */

  return 0;
}

int Joruus::h_433 (Joruus::TEvent* evt)
{
  /* ERR_NICKNAMEINUSE
  Param�tres : <nick>
  R�ponse � : NICK
  Cette erreur est renvoy�e quand le nick est d�j� utilis� sur le r�seau. */

  return 0;
}

int Joruus::h_436 (Joruus::TEvent* evt)
{
  /* ERR_NICKCOLLISION
  Param�tres : <nick>
  R�ponse � : NICK
  Cette erreur est renvoy�e quand le nick est d�j� utilis� sur un autre serveur. */

  return 0;
}

int Joruus::h_442 (Joruus::TEvent* evt)
{
  /* ERR_NOTONCHANNEL
  Param�tres : <chan>
  R�ponse � : PART, MODE, INVITE, TOPIC, KICK
  Cette erreur est renvoy�e quand vous n'�tes pas pr�sent sur le chan. */

  return 0;
}

int Joruus::h_443 (Joruus::TEvent* evt)
{
  /* ERR_USERONCHANNEL
  Param�tres : <nick>
  R�ponse � : INVITE
  Cette erreur est renvoy�e quand le nick est d�j� sur le chan. */

  return 0;
}

int Joruus::h_461 (Joruus::TEvent* evt)
{
  /* ERR_NEEDMOREPARAMS
  Param�tres : <commande>
  R�ponse � : PASS, USER, OPER, KILL, JOIN, PART, MODE, TOPIC, INVITE, KICK
  Cette erreur est renvoy�e quand il manque un param�tre � la commande. */

  return 0;
}

int Joruus::h_462 (Joruus::TEvent* evt)
{
  /* ERR_ALREADYREGISTRED
  Param�tres : <>
  R�ponse � : PASS, USER, SERVER
  Cette erreur est renvoy�e quand la connection a d�j� �t� identifi�e. */

  return 0;
}

int Joruus::h_464 (Joruus::TEvent* evt)
{
  /* ERR_PASSWDMISMATCH
  Param�tres : <>
  R�ponse � : OPER
  Cette erreur est renvoy�e quand le mot de passe pour devenir IRCOP est incorect. */

  return 0;
}

int Joruus::h_467 (Joruus::TEvent* evt)
{
  /* ERR_KEYSET
  Param�tres : <chan>
  R�ponse � : MODE
  Cette erreur est renvoy�e quand la cl� du chan est d�j� d�finie. */

  return 0;
}

int Joruus::h_471 (Joruus::TEvent* evt)
{
  /* ERR_CHANNELISFULL
  Param�tres : <chan>
  R�ponse � : JOIN
  Cette erreur est renvoy�e quand la limite de nombres d'utilisateurs pr�sents sur le chan a d�j� �t� atteinte. */

  return 0;
}

int Joruus::h_472 (Joruus::TEvent* evt)
{
  /* ERR_UNKNOWNMODE
  Param�tres : <caract�re>
  R�ponse � : MODE
  Cette erreur est renvoy�e quand le un code de mode de chan inconnu est utilis�. */

  return 0;
}

int Joruus::h_473 (Joruus::TEvent* evt)
{
  /* ERR_INVITEONLYCHAN
  Param�tres : <chan>
  R�ponse � : JOIN
  Cette erreur est renvoy�e quand le chan n'est joignable que sur invitation (et que vous n'�tes pas invit�). */

  return 0;
}

int Joruus::h_474 (Joruus::TEvent* evt)
{
  /* ERR_BANNEDFROMCHAN
  Param�tres : <chan>
  R�ponse � : JOIN
  Cette erreur est renvoy�e quand vous tentez d'entrer sur un chan dont vous avez �t� banni. */

  return 0;
}

int Joruus::h_475 (Joruus::TEvent* evt)
{
  /* ERR_BADCHANNELKEY
  Param�tres : <chan>
  R�ponse � : JOIN
  Cette erreur est renvoy�e quand la cl� sp�cifi�e pour le chan est invalide. */

  return 0;
}

int Joruus::h_481 (Joruus::TEvent* evt)
{
  /* ERR_NOPRIVILEGES
  Param�tres : <>
  R�ponse � : SQUIT, KILL
  Cette erreur est renvoy�e quand vous tentez d'ex�cuter des commandes r�serv�es aux IRCOPs alors que
  vous n'en avez pas le status. */

  return 0;
}

int Joruus::h_482 (Joruus::TEvent* evt)
{
  /* ERR_CHANOPRIVSNEEDED
  Param�tres : <chan>
  R�ponse � : MODE, TOPIC, INVITE, KICK
  Cette erreur est renvoy�e quand vous tentez d'effectuer une op�ration d'administration sur un chan o� vous
  n'�tes pas op. */

  return 0;
}

int Joruus::h_483 (Joruus::TEvent* evt)
{
  /* ERR_CANNTKILLSERVER
  Param�tres : <>
  R�ponse � : KILL
  Cette erreur est renvoy�e quand vous tentez d'ex�cuter une commande KILL sur un serveur au lieu d'un
  utilisateur. */

  return 0;
}

int Joruus::h_491 (Joruus::TEvent* evt)
{
  /* ERR_NOOPERHOST
  Param�tres : <>
  R�ponse � : OPER
  Cette erreur est renvoy�e quand votre domaine n'est pas enregistr� pour acc�der au rang d'IRCOP malgr�s
  votre tentative d'identification en tant que tel. */

  return 0;
}
  
int Joruus::h_501 (Joruus::TEvent* evt)
{
  /* ERR_UMODUUNKNOWNFLAG
  Param�tres : <>
  R�ponse � : MODE
  Cette erreur est renvoy�e quand vous envoyez une commande MODE sur un utilisateur avec un mode
  inconnu. */

  return 0;
}

int Joruus::h_502 (Joruus::TEvent* evt)
{
  /* ERR_USERSDONTMATCH
  Param�tres : <>
  R�ponse � : MODE
  Cette erreur est renvoy�e quand vous tentez d'effectuer une commande MODE sur un nick autre que le votre. */

  return 0;
}


int Joruus::h_CONSOLE_MSG (Joruus::TEvent* evt)
{
  //cout << "'" << evt->Data.c_str () << "'" << endl << flush;
  //cout << evt->Server->getHostname () << endl;
  string Message;

  if (evt != nullptr)
  {
    if (evt->Server != nullptr)
    {
      Message = "PRIVMSG "+evt->Server->getCurrentInterlocutor()+" :"+evt->Data;
      evt->Server->getConnection()->IrcSendMessage (evt->Data);
      //evt->Server->IrcSendMessage (Message);
    }
  }
  return 0;
}


int Joruus::h_UNKNOWN_CMD (Joruus::TEvent* evt)
{
  return 0;
}
