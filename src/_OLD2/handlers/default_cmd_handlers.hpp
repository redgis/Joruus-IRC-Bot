/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _DEFAULT_CMD_HANDLERS_H
#define _DEFAULT_CMD_HANDLERS_H

#include "../joruus.hpp"


/**** TO COMPLETE ****/

namespace Joruus
{
  /* Declaration of default handlers */
  int h_cmdCONNECT (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdJOIN (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdMSG (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdNICK (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdPART (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdTALKTO (string & Arguments, CCommand * Command, int argc, char * argv[]);
  int h_cmdUSESERVER (string & Arguments, CCommand * Command, int argc, char * argv[]);

  /* defines of default commands : */
  extern int CommandNbArgs[][2];
  extern const char * Command[][3];
  
  /* defines of default Commands : */
  extern TCommandHandler CommandDefaultHandler[];
  
};


#endif /* _DEFAULT_CMD_HANDLERS_H */

