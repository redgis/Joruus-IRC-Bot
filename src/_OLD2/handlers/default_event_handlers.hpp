/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _DEFAULT_EVENT_HANDLERS_H
#define _DEFAULT_EVENT_HANDLERS_H

#include "../joruus.hpp"


/**** TO COMPLETE ****/

namespace Joruus
{
  /* Declaration of default handlers */

    // Basics
  int h_UNKNOWN (Joruus::TEvent*);
  int h_PASS (Joruus::TEvent*);
  int h_NICK (Joruus::TEvent*);
  int h_USER (Joruus::TEvent*);
  int h_SERVER (Joruus::TEvent*);
  int h_QUIT (Joruus::TEvent*);
  int h_SQUIT (Joruus::TEvent*);
  int h_PING (Joruus::TEvent*);
  int h_PONG (Joruus::TEvent*);
  int h_OPER (Joruus::TEvent*);
  int h_KILL (Joruus::TEvent*);
  int h_ERROR (Joruus::TEvent*);
  int h_JOIN (Joruus::TEvent*);
  int h_PART (Joruus::TEvent*);
  int h_MODE (Joruus::TEvent*);
  int h_TOPIC (Joruus::TEvent*);
  int h_NAMES (Joruus::TEvent*);
  int h_LIST (Joruus::TEvent*);
  int h_INVITE (Joruus::TEvent*);
  int h_KICK (Joruus::TEvent*);
  int h_WHOIS (Joruus::TEvent*);
  int h_WHOWAS (Joruus::TEvent*);
  int h_NOTICE (Joruus::TEvent*);
  int h_PRIVMSG (Joruus::TEvent*);

    //Replies
  int h_221 (Joruus::TEvent*);
  int h_311 (Joruus::TEvent*);
  int h_221 (Joruus::TEvent*);
  int h_311 (Joruus::TEvent*);
  int h_312 (Joruus::TEvent*);
  int h_313 (Joruus::TEvent*);
  int h_314 (Joruus::TEvent*);
  int h_317 (Joruus::TEvent*);
  int h_318 (Joruus::TEvent*);
  int h_319 (Joruus::TEvent*);
  int h_322 (Joruus::TEvent*);
  int h_323 (Joruus::TEvent*);
  int h_324 (Joruus::TEvent*);
  int h_331 (Joruus::TEvent*);
  int h_332 (Joruus::TEvent*);
  int h_341 (Joruus::TEvent*);
  int h_353 (Joruus::TEvent*);
  int h_366 (Joruus::TEvent*);
  int h_367 (Joruus::TEvent*);
  int h_368 (Joruus::TEvent*);
  int h_369 (Joruus::TEvent*);
  int h_381 (Joruus::TEvent*);

    //Errors
  int h_401 (Joruus::TEvent*);
  int h_402 (Joruus::TEvent*);
  int h_403 (Joruus::TEvent*);
  int h_404 (Joruus::TEvent*);
  int h_406 (Joruus::TEvent*);
  int h_409 (Joruus::TEvent*);
  int h_411 (Joruus::TEvent*);
  int h_412 (Joruus::TEvent*);
  int h_413 (Joruus::TEvent*);
  int h_414 (Joruus::TEvent*);
  int h_431 (Joruus::TEvent*);
  int h_432 (Joruus::TEvent*);
  int h_433 (Joruus::TEvent*);
  int h_436 (Joruus::TEvent*);
  int h_442 (Joruus::TEvent*);
  int h_443 (Joruus::TEvent*);
  int h_461 (Joruus::TEvent*);
  int h_462 (Joruus::TEvent*);
  int h_464 (Joruus::TEvent*);
  int h_467 (Joruus::TEvent*);
  int h_471 (Joruus::TEvent*);
  int h_472 (Joruus::TEvent*);
  int h_473 (Joruus::TEvent*);
  int h_474 (Joruus::TEvent*);
  int h_475 (Joruus::TEvent*);
  int h_481 (Joruus::TEvent*);
  int h_482 (Joruus::TEvent*);
  int h_483 (Joruus::TEvent*);
  int h_491 (Joruus::TEvent*);
  int h_501 (Joruus::TEvent*);
  int h_502 (Joruus::TEvent*);

    //Others
  int h_CONSOLE_MSG (Joruus::TEvent*);
  int h_UNKNOWN_CMD (Joruus::TEvent*);

  /* defines of default events : */
  extern const char * EventTypeKeywordsAndDescription[][2];
  
  /* defines of default events : */
  extern TEventHandler EventTypeDefaultHandler[];
  

};


#endif /* _DEFAULT_EVENT_HANDLERS_H */

  /* Indice in the array corresponds to the event ID */ /*
  enum eDefaultEventType{
  // Basics
    EVT_UNKNOWN,
    EVT_PASS,
    EVT_NICK,
    EVT_USER,
    EVT_SERVER,
    EVT_QUIT,
    EVT_SQUIT,
    EVT_PING,
    EVT_PONG,
    EVT_OPER,
    EVT_KILL,
    EVT_ERROR,
    EVT_JOIN,
    EVT_PART,
    EVT_MODE,
    EVT_TOPIC,
    EVT_NAMES,
    EVT_LIST,
    EVT_INVITE,
    EVT_KICK,
    EVT_WHOIS,
    EVT_WHOWAS,
    EVT_NOTICE,
    EVT_PRIVMSG,

//Replies
    EVT_221,
    EVT_311,
    EVT_312,
    EVT_313,
    EVT_314,
    EVT_317,
    EVT_318,
    EVT_319,
    EVT_322,
    EVT_323,
    EVT_324,
    EVT_331,
    EVT_332,
    EVT_341,
    EVT_353,
    EVT_366,
    EVT_367,
    EVT_368,
    EVT_369,
    EVT_381,

//Errors
    EVT_401,
    EVT_402,
    EVT_403,
    EVT_404,
    EVT_406,
    EVT_409,
    EVT_411,
    EVT_412,
    EVT_413,
    EVT_414,
    EVT_431,
    EVT_432,
    EVT_433,
    EVT_436,
    EVT_442,
    EVT_443,
    EVT_461,
    EVT_462,
    EVT_464,
    EVT_467,
    EVT_471,
    EVT_472,
    EVT_473,
    EVT_474,
    EVT_475,
    EVT_481,
    EVT_482,
    EVT_483,
    EVT_491,
    EVT_501,
    EVT_502,

//Others
    EVT_CONSOLE_MSG,
    EVT_CONNECT
  };
*/

/*

//Basics

UNKNOWN
PASS
NICK
USER
SERVER
QUIT
SQUIT
PING
PONG
OPER
KILL
ERROR
JOIN
PART
MODE
TOPIC
NAMES
LIST
INVITE
KICK
WHOIS
WHOWAS
NOTICE
PRIVMSG

//Replies

221
311
312
313
314
317
318
319
322
323
324
331
332
341
353
366
367
368
369
381

//Errors

401
402
403
404
406
409
411
412
413
414
431
432
433
436
442
443
461
462
464
467
471
472
473
474
475
481
482
483
491
501
502

//Others

CONSOLE_MSG
CONNECT

*/
