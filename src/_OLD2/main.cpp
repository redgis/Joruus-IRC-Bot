/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "joruus.hpp"

#include "bot.hpp"
#include "eventmanager.hpp"



int main (int argc, char * argv[])
{

  //cout << "\"" << "\x20" << "\"" << endl;
  //exit(1);

  if (argc != 1)
  {
    cout << "Usage: " << argv[0] << endl;
    exit (1);
  }
  
  Joruus::EventManager = new Joruus::CEventManager ();

  Joruus::init ();
  JoruusCBot = new Joruus::CBot ();
  
  /* Start main loop */
  JoruusCBot->create ();

  //Waiting for the end of the thread.
  JoruusCBot->join ();
  
  return 1;
}
