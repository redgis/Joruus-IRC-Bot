/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef UDPSERVERCONNEXION_HPP_
#define UDPSERVERCONNEXION_HPP_

#include "../joruus.hpp"

/*****************************************************************************/
class Joruus::CServer_UDPConnexion : public Joruus::CServerConnexion
{
  public:
    CServer_UDPConnexion(int BufferSize);
    virtual ~CServer_UDPConnexion();

    virtual int Init (unsigned short int LocalPort, unsigned long int LocalAdrIP = INADDR_ANY);
};

#endif /* UDPSERVERCONNEXION_HPP_ */
