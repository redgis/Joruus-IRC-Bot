/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "serverconnexion.hpp"

/*****************************************************************************/
Joruus::CServerConnexion::CServerConnexion(int BufferSize) : CConnexion(BufferSize)
{
}

/*****************************************************************************/
Joruus::CServerConnexion::~CServerConnexion()
{
}

/*****************************************************************************/
int Joruus::CServerConnexion::Init (unsigned short int LocalPort, unsigned long int LocalAdrIP)
{
  m_LocalIP = LocalAdrIP;
  m_LocalPort = LocalPort;

  return 0;
}

/*****************************************************************************/
int Joruus::CServerConnexion::Listen ()
{
  int Res = 0;

  if (m_ConnexionState != CNX_INITIALIZED)
  {
    Joruus::sysMessagePrint ("Socket must be initialized before listening", (char *) __FILE__, __LINE__);
    return -1;
  }
  /* On met la socket en ecoute */
  Res = listen (m_Socket, 5);
  if (Res == -1)
    Joruus::sysMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
  else
    m_ConnexionState = CNX_LISTEN;

  return m_Socket;
}

