/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef TCPSERVERCONNEXION_HPP_
#define TCPSERVERCONNEXION_HPP_

#include "../joruus.hpp"

/*****************************************************************************/
class Joruus::CServer_TCPConnexion : public Joruus::CServerConnexion
{
  public:
    CServer_TCPConnexion(int BufferSize);
    virtual ~CServer_TCPConnexion();

    virtual int Init (unsigned short int LocalPort, unsigned long int LocalAdrIP = INADDR_ANY);
    virtual CClient_TCPConnexion * Accept ();
};

#endif /* TCPSERVERCONNEXION_HPP_ */
