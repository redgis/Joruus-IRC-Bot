/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _CLIENT_TCP_CONNEXION_H
#define _CLIENT_TCP_CONNEXION_H

#include "../joruus.hpp"

/***************************************************************************/
class Joruus::CClient_TCPConnexion : public Joruus::CClientConnexion
{
  protected:

    
  public:
    /* Constructors / destructors */
    CClient_TCPConnexion (int BufferSize);
    virtual ~CClient_TCPConnexion ();

    
    virtual int Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP = INADDR_ANY);
    virtual int Connect ();

    virtual int Send (const char * Data);

};

#endif /* _CLIENT_TCP_CONNEXION_H */
