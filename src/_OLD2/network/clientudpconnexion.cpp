/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "clientudpconnexion.hpp"

Joruus::CClient_UDPConnection::CClient_UDPConnection()
{

}

Joruus::CClient_UDPConnection::~CClient_UDPConnection()
{

}


int Joruus::CClient_UDPConnection::Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP)
{
  int Res = Joruus::CClientConnexion::Init(RemoteAdrIP,RemotePort);
  struct sockaddr_in RemoteAdr;
  struct sockaddr_in LocalAdr;


  /* initialisation les donnees pour l'adresse serveur */
  RemoteAdr.sin_family = AF_INET;
  RemoteAdr.sin_port = htons (m_RemoteIP);
  RemoteAdr.sin_addr.s_addr = htonl(m_RemoteIP);

  /* initialisation les donnees pour l'adresse locale */
  LocalAdr.sin_family = AF_INET;
  LocalAdr.sin_addr.s_addr = htonl(INADDR_ANY);
  LocalAdr.sin_port = htons(0);

  if (m_ConnexionState != CNX_CLOSED)
    Close();

  /* Creation socket */
  m_Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if(m_Socket == -1)
  {
    debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
  }

  /* On associe la socket au port */
  Res = bind (m_Socket, (struct sockaddr *) &LocalAdr, sizeof(LocalAdr));
  if (Res == -1)
  {
    debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
  }
  else
  {
    m_LocalIP = LocalAdr.sin_addr;
    m_LocalPort = LocalAdr.sin_port;
    m_ConnexionState = CNX_INITIALIZED;
  }

  return m_Socket;
}


int Joruus::CClient_UDPConnection::Send (const char * Data)
{
  int Size = strlen (Data);
  int Sent = 0, ToSend = Size;
  bool Exit;
  int Res = 0;

  struct sockaddr_in RemoteAdr;

  /* initialisation les donnees pour l'adresse serveur */
  RemoteAdr.sin_family = AF_INET;
  RemoteAdr.sin_port = htons (m_RemoteIP);
  RemoteAdr.sin_addr.s_addr = htonl(m_RemoteIP);

  if (m_ConnexionState != CNX_INITIALIZED)
  {
    debugMessagePrint ("Must be initialized to send data", (char *) __FILE__, __LINE__);
    return -1;
  }

  if (m_Socket != -1)
  {
    while (!Exit)
    {

      Res = sendto(m_Socket, Data+Sent, Size, 0, (struct sockaddr *) &RemoteAdr, sizeof(RemoteAdr));

      Sent += Res;

      ToSend = Size - Sent;
      if (Sent == Size)
      {
        Exit = true;
        Res = Sent;
      }

      if (Res == -1)
        Exit = true;
    }

  }

  return Sent;

}

int Joruus::CClient_UDPConnection::SendTo (const char * Data, unsigned long int RemoteAdrIP, unsigned short int RemotePort)
{
  int Res = Init (RemoteAdrIP, RemotePort);
  return (Res == -1)?Res:Send(Data);
}
