/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "servertcpconnexion.hpp"
#include "serverconnexion.hpp"

/*****************************************************************************/
Joruus::CServer_TCPConnexion::CServer_TCPConnexion(int BufferSize) : CServerConnexion(BufferSize)
{
  // TODO Auto-generated constructor stub

}

/*****************************************************************************/
Joruus::CServer_TCPConnexion::~CServer_TCPConnexion()
{
  // TODO Auto-generated destructor stub
}

/*****************************************************************************/
int Joruus::CServer_TCPConnexion::Init (unsigned short int LocalPort, unsigned long int LocalAdrIP)
{
  struct sockaddr_in ServAdr;
  int SizeAdr;
  int Res = Joruus::CServerConnexion::Init(LocalPort, LocalAdrIP);

  /* initialisation des donnees pour la connexion serveur */
  ServAdr.sin_family = AF_INET;
  ServAdr.sin_port = htons (m_LocalPort);
  ServAdr.sin_addr.s_addr = htonl(m_LocalIP);

  SizeAdr = sizeof (ServAdr);

  /* creation de la socket */
  m_Socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (m_Socket == -1)
  {
    Joruus::sysMessagePrint ((string ("Unable to open listening TCP socket") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    m_ConnexionState = CNX_CLOSED;
  }

  /* On lie la socket au port voulu, sur l'IP voulue */
  Res = bind (m_Socket, (const struct sockaddr*) &ServAdr, SizeAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint ((string("Unable to bind listening TCP socket on port: ") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    m_ConnexionState = CNX_CLOSED;
  }
  else
  {
    m_LocalIP = ServAdr.sin_addr;
    m_LocalPort = ntohs(ServAdr.sin_port);
    m_ConnexionState = CNX_INITIALIZED;
  }

  return Res;
}

/*****************************************************************************/
CClient_TCPConnexion * Joruus::CServer_TCPConnexion::Accept ()
{
  int Res;
  struct sockaddr_in Adr;
  int SizeAdr = sizeof(Adr);
  struct sockaddr_in LocalAdr;
  int SizeLocalAdr = sizeof(Adr);

  // Check connexion status ready
  if (m_ConnexionState != CNX_LISTEN)
  {
    Joruus::sysMessagePrint ("Socket must be listening to accept a connexion", (char *) __FILE__, __LINE__);
    return nullptr;
  }

  //Create connection object to store new accepted socket
  CClient_TCPConnexion * newConnexion = new CClientConnexion (m_BufferSize);

  newConnexion->m_Socket = accept(m_Socket, (struct sockaddr *) &Adr, (socklen_t *) &SizeAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
    delete newConnexion;
    return nullptr;
  }
  else
  {
    newConnexion->m_RemoteIP = Adr.sin_addr;
    newConnexion->m_RemotePort = ntohs(Adr.sin_port);
    newConnexion->m_ConnexionState = CNX_CONNECTED;
  }

  // Get local address to fill m_LocalIP & port
  Res = getsockname(newConnexion->m_Socket, (struct sockaddr *) &LocalAdr, (socklen_t *) &SizeLocalAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint ((string("Unable to retrieve local address of accepted socket: ") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    delete newConnexion;
    return nullptr;
  }
  else
  {
    newConnexion->m_LocalIP = LocalAdr.sin_addr;
    newConnexion->m_LocalPort = ntohs(LocalAdr.sin_port);
  }

  return newConnexion;
}
