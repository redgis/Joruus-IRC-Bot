/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "clientircconnexion.hpp"
#include "../irc/user.hpp"
#include "clienttcpconnexion.hpp"

/***************************************************************************/
Joruus::CClient_IRCConnexion::CClient_IRCConnexion (int BufferSize, string targetHost, unsigned short int newPort, CBot * myOwner) : Joruus::CClient_TCPConnexion::CClient_TCPConnexion (BufferSsize)
{
  m_BotUser = myOwner;
  m_TargetHost = targetHost;

  Init (newPort, GetIP (m_TargetHost.c_str()));
}

/***************************************************************************/
Joruus::CClient_IRCConnexion::~CClient_IRCConnexion ()
{
}

/***************************************************************************/
int Joruus::CClient_IRCConnexion::Send (const char * Message)
{
  int res;

  string Output = Message;
  Output += IRC_CRLF;

  debugMessagePrint (Message, (char *) __FILE__, __LINE__, time(nullptr));
  res = CClient_TCPConnexion::Send(Output.c_str ());

  return res;
}

///***************************************************************************/
//int Joruus::CClient_TCPConnexion::Recieve (const char * buf, size_t count)
//{
//
//  while ((Res = recv (Sock, IncomingData, BUFFER_MAX_SIZE, 0)) > 0)
//  {
//    IncomingData[Res] = '\0';
//    //cout << IncomingData << "\\" << endl;
//
//    /* Dispatch the message if it comes from a known client (otherwise, ignore it) */
//    if (Res != 0)
//    {
//      Message.Origine = Sock;
//      Message.Server = (*itCurrentServer)->m_Server;
//
//      m_Buffers[Sock] += string ((const char *) IncomingData);
//      TmpMessage = m_Buffers[Sock];
//
//      while (TmpMessage.find(IRC_CRLF) != string::npos)
//      {
//        Message.Content = TmpMessage.substr (0, TmpMessage.find(IRC_CRLF));
//
//        TmpMessage = TmpMessage.substr (TmpMessage.find(IRC_CRLF) + strlen(IRC_CRLF));
//
//        //cout << "\"" << Message.Content << "\"" << endl;
//        Event = parseMessage (Message);
//      }
//      m_Buffers[Sock] = TmpMessage;
//    }
//  } /* while */
//}

/***************************************************************************/
int Joruus::CClient_IRCConnexion::Connect ()
{
  int result = Joruus::CClient_TCPConnexion::Connect ();

  if (result == -1)
    sysMessagePrint(strerror(errno), (char *)__FILE__, __LINE__, nullptr);
  else
  {
    if (getServer () == nullptr)

    m_Server = new CServer(this);
    m_Server.addUser(&m_BotUser);

    /* Identification to server */
    string Message = "NICK " + m_BotUser.getNick ();
    Send (Message.c_str());

    //sleep (1);
    Message = "USER "+m_BotUser.getName() + " " + m_BotUser.getDomain() + " " + m_TargetHost + " " + m_BotUser.getRealName();
    Send (Message.c_str());
  }
  return result;
}

/***************************************************************************/
CServer * Joruus::CClient_IRCConnexion::getServer ()
{
  return &m_BotUser;
}
