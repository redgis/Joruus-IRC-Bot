/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */


#include "clienttcpconnexion.hpp"


/***************************************************************************/
Joruus::CClient_TCPConnexion::CClient_TCPConnexion (int BufferSize) : Joruus::CConnexion::CConnexion(BufferSize)
{

}

/***************************************************************************/
Joruus::CClient_TCPConnexion::~CClient_TCPConnexion ()
{
}

/***************************************************************************/
int Joruus::CClient_TCPConnexion::Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP)
{
  int SizeLocalAdr, SizeRemoteAdr;
  struct sockaddr_in LocalAdr;
  struct sockaddr_in RemoteAdr;
  int Res;

  m_RemoteIP = RemoteAdrIP;
  m_RemotePort = RemotePort;

  //printf (" %u... \n", AdrIP);

  /* initialisation les donnees pour l'adresse locale */
  LocalAdr.sin_family = AF_INET;
  LocalAdr.sin_port = htons (0);  /* en local : n'importe quel port ! */
  LocalAdr.sin_addr.s_addr = htonl(INADDR_ANY);

  /* initialisation les donnees pour l'adresse serveur */
  RemoteAdr.sin_family = AF_INET;
  RemoteAdr.sin_port = htons (RemotePort);
  RemoteAdr.sin_addr.s_addr = /*htonl(*/RemoteAdrIP/*)*/;

  SizeLocalAdr = sizeof (LocalAdr);
  SizeRemoteAdr = sizeof (RemoteAdr);

  /* Creation socket */
  m_Socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (m_Socket == -1)
  {
    debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
    return -1;
  }

  /* On associe la socket a l'adresse et au port local */
  Res = bind (m_Socket, (struct sockaddr *) &LocalAdr, sizeof(LocalAdr));
  if (Res == -1)
  {
    debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
  }
  else
  {
    m_LocalIP = LocalAdr.sin_addr;
    m_LocalPort = ntohs (LocalAdr.sin_port);
    m_ConnexionState = CNX_INITIALIZED;
  }


  return Res;
}

/***************************************************************************/
/* TCP connect to a listening IP address / port. Returns the socket */
int Joruus::CClient_TCPConnexion::Connect ()
{
  int Res = 0;
  struct sockaddr_in RemoteAdr;

  if (m_ConnexionState != CNX_INITIALIZED)
  {
    debugMessagePrint ("Unable to connect, socket not initialized", (char *)__FILE__, __LINE__);
    Res = -1;
  }
  else
  {
    /* initialisation les donnees pour l'adresse serveur */
    RemoteAdr.sin_family = AF_INET;
    RemoteAdr.sin_port = htons (m_RemotePort);
    RemoteAdr.sin_addr.s_addr = /*htonl(*/m_RemoteIP/*)*/;

    /* On se connecte a l'adresse et au port distants */
    Res = connect (m_Socket, (struct sockaddr *) &RemoteAdr, sizeof(RemoteAdr));
    if (Res == -1)
    {
      debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
      Close();
    }
    else
      m_ConnexionState = CNX_CONNECTED;
  }

  return Res;

}

/***************************************************************************/
int Joruus::CClient_TCPConnexion::Send (const char * Data)
{
  int Size = strlen (Data);
  int Sent = 0, ToSend = Size;
  bool Exit = false;
  int Res = 0;

  if (m_ConnexionState != CNX_CONNECTED)
  {
    debugMessagePrint ("Must be connected to send data", (char *) __FILE__, __LINE__);
    return -1;
  }

  do
  {
    Res = write (m_Socket, Data+Sent, ToSend);

    Sent += Res;
    ToSend = Size - Sent;

  } while (ToSend > 0);

  return Sent;
}


