/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "serverudpconnexion.hpp"
#include "serverconnexion.hpp"

/*****************************************************************************/
Joruus::CServer_UDPConnexion::CServer_UDPConnexion(int BufferSize) : CServerConnexion(BufferSize)
{
  // TODO Auto-generated constructor stub

}

/*****************************************************************************/
Joruus::CServer_UDPConnexion::~CServer_UDPConnexion()
{
  // TODO Auto-generated destructor stub
}

/*****************************************************************************/
int Joruus::CServer_UDPConnexion::Init (unsigned short int LocalPort, unsigned long int LocalAdrIP)
{
  struct sockaddr_in ServAdr;
  int SizeAdr;
  int Res = Joruus::CServerConnexion::Init(LocalPort, LocalAdrIP);

  /* initialisation des donnees pour la connexion serveur */
  ServAdr.sin_family = AF_INET;
  ServAdr.sin_port = htons (m_LocalPort);
  ServAdr.sin_addr.s_addr = htonl(m_LocalIP);

  SizeAdr = sizeof (ServAdr);

  /* creation de la socket */
  m_Socket = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (m_Socket == -1)
  {
    Joruus::sysMessagePrint ((string ("Unable to open listening TCP socket") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    m_ConnexionState = CNX_CLOSED;
  }

  /* On lie la socket au port voulu, sur l'IP voulue */
  Res = bind (m_Socket, (const struct sockaddr*) &ServAdr, SizeAdr);
  if (Res == -1)
  {
    Joruus::sysMessagePrint ((string("Unable to bind listening TCP socket on port: ") + strerror(errno)).c_str (), (char *) __FILE__, __LINE__);
    m_ConnexionState = CNX_CLOSED;
  }
  else
  {
    m_LocalIP = ServAdr.sin_addr;
    m_LocalPort = ntohs(ServAdr.sin_port);
    m_ConnexionState = CNX_INITIALIZED;
  }

  return Res;
}
