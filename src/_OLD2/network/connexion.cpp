/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "connexion.hpp"

/***************************************************************************/
Joruus::CConnexion::CConnexion(int BufferSize) : m_BufferSize(BufferSize)
{
  m_Buffer = new char[m_BufferSize];
  m_LocalIP = 0;
  m_LocalPort = 0;
  m_ConnexionState = CNX_CLOSED;
}

/***************************************************************************/
Joruus::CConnexion::~CConnexion()
{
  Close ();
  if (m_Buffer != nullptr)
    delete m_Buffer;
}

/***************************************************************************/
int Joruus::CConnexion::Close ()
{
  m_ConnexionState = CNX_CLOSED;

  if (m_Socket != -1)
    return close (m_Socket);
  else return -1;
}

/***************************************************************************/
int Joruus::CConnexion::Recieve (int max_size)
{
  if (m_Buffer != nullptr)
    return read (m_Socket, m_Buffer, max_size);
  else
    return -1;
}

/***************************************************************************/
int Joruus::CConnexion::setBlocking (bool yesorno)
{
  if ((m_ConnexionState == CNX_CONNECTED) or (m_ConnexionState == CNX_INITIALIZED))
  {
    if (yesorno)
    {
      /* On passe la socket en mode non bloquant. idem pour l'entree clavier */
      return fcntl (m_Socket, F_SETFL, O_NONBLOCK | fcntl (m_Socket, F_GETFL));
    }
    else
    {
      return fcntl (m_Socket, F_SETFL, fcntl (m_Socket, F_GETFL) & (~O_NONBLOCK));
    }
  }
  else
    return -1;
}

