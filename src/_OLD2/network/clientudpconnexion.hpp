/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef UDPCLIENTCONNECTION_HPP_
#define UDPCLIENTCONNECTION_HPP_

#include "../joruus.hpp"


/***************************************************************************/
class Joruus::CClient_UDPConnection : public Joruus::CClientConnexion
{
  protected:


  public:
    CClient_UDPConnection();
    virtual ~CClient_UDPConnection();

    virtual int Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP = INADDR_ANY);

    virtual int Close ();
    virtual int Send (const char * Data);
    virtual int SendTo (const char * Data, unsigned long int RemoteAdrIP, unsigned short int RemotePort = INADDR_ANY);
};

#endif /* UDPCLIENTCONNECTION_HPP_ */
