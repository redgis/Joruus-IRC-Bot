/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef IRCCLIENTCONNECTION_HPP_
#define IRCCLIENTCONNECTION_HPP_



#include "../joruus.hpp"

class Joruus::CClient_IRCConnexion : public Joruus::CClient_TCPConnexion
{
  private:
    string m_TargetHost;

  public:
    CUser m_BotUser;          /* Bot info associated with to this server */
    CServer m_Server;         /* Server*/
    string m_Interlocutor;      /* Channel or user currently talking on / to */

    /* Constructors / destructors */
    CClient_IRCConnexion (int BufferSize = 1024, string targetHost = "127.0.0.1", unsigned short int newPort = 6667, Joruus::CBot * myOwnerBot = nullptr);
    virtual ~CClient_IRCConnexion ();

    virtual int Send (const char * Data);
    virtual int Recieve (int max_size);

    virtual int Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP = INADDR_ANY);
    virtual int Connect ();

    CServer * getServer ();
};

#endif /* IRCCLIENTCONNECTION_HPP_ */
