/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef CLIENTCONNEXION_HPP_
#define CLIENTCONNEXION_HPP_

#include "../joruus.hpp"

/***************************************************************************/
class Joruus::CClientConnexion : public Joruus::CConnexion
{
  protected:
    unsigned short int m_RemotePort;
    unsigned long int m_RemoteIP;

  public:
    CClientConnexion(int BufferSize);
    virtual ~CClientConnexion();

    virtual int Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP = INADDR_ANY);
};

#endif /* CLIENTCONNEXION_HPP_ */
