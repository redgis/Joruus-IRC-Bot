/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#include "clientconnexion.hpp"

Joruus::CClientConnexion::CClientConnexion(int BufferSize) : CConnexion(BufferSize)
{
  m_RemoteIP = 0;
  m_RemotePort = 0;
}

Joruus::CClientConnexion::~CClientConnexion()
{

}


int Joruus::CClientConnexion::Init (unsigned short int RemotePort, unsigned long int RemoteAdrIP)
{
  m_RemoteIP = RemoteAdrIP;
  m_RemotePort = RemotePort;

  return 0;
}
