/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef SERVERCONNEXION_HPP_
#define SERVERCONNEXION_HPP_

#include "../joruus.hpp"

/*****************************************************************************/
class Joruus::CServerConnexion : public Joruus::CConnexion
{
  protected:

  public:
    CServerConnexion(int BufferSize);
    virtual ~CServerConnexion();

    virtual int Init (unsigned short int LocalPort, unsigned long int LocalAdrIP = INADDR_ANY);
    virtual int Listen ();
};

#endif /* SERVERCONNEXION_HPP_ */
