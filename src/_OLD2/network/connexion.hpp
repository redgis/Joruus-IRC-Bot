/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef CONNEXION_HPP_
#define CONNEXION_HPP_

#include "../joruus.hpp"


namespace Joruus {
	namespace Network {
typedef enum eConnexionState { CNX_CLOSED, CNX_INITIALIZED, CNX_CONNECTED, CNX_LISTEN} TConnexionState;


/***************************************************************************/
class CConnexion {
  protected:
    unsigned short int m_LocalPort;
    unsigned long int m_LocalIP;

    int m_Socket;

    char * m_Buffer;
    int m_BufferSize;

    Joruus::TConnexionState m_ConnexionState;

  public:
    CConnexion(int BufferSize);
    virtual ~CConnexion();

    virtual int Init (unsigned short int Port, unsigned long int AdrIP = INADDR_ANY) = 0;

    virtual int Close ();
    virtual int Send (const char * Data) = 0;
    virtual int Recieve (int max_size);

    int setBlocking (bool yesorno);

    /*****************************************************************************/
    /* resolution DNS : on veut l'IP en fonction du parametre en chaine de car.
     * (a nous de trouver si c'est une IP ou un DNS) */
    unsigned long int GetIP (const char * Host);
    /*****************************************************************************/
    /* resolution DNS inverse : on veut le nom de domaine en fonction de l'IP */
    char * GetDNS (unsigned long int AdrIp);

};

} //namespace Network
} //namespace Joruus

#endif /* CONNEXION_HPP_ */
