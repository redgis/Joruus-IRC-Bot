/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _BOT_H
#define _BOT_H

#include "joruus.hpp"

/***************************************************************************/
class Joruus::CBot : public Joruus::CThread
{
  private:
    vector <CClient_IRCConnexion *> m_IrcConnexions;     /* Servers  list and associated buffers */
    CClient_IRCConnexion * m_CurrentServer;               /* Server on which we are currently talking */
    Joruus::CEventManager * m_EventManager;                /* Event Manager */
    
    /* Parse string recieved or sent, and transform it into a CEvent to be posted to event manager. */
    Joruus::CEvent * parseMessage (Joruus::CIrcMessage & Message);
  


    /* Fill the FD Set */
    int FillFdSet ();
    fd_set m_ReadFd;
    fd_set m_ErrorFd;

  public:
    /* Constructors / destructors */
    CBot ();
    virtual ~CBot ();

    /* inherited from CThread, to be implemented */
    virtual void run ();

    /* Accessors */
    void addServerConnection (CClient_IRCConnexion *);
    bool removeServerConnection (CClient_IRCConnexion *);

    /* Manage server we are currently talking to */
    Joruus::CClient_IRCConnexion * getCurrentServer ();
    void setCurrentServer (CClient_IRCConnexion *);
    void setCurrentServer (int);

    /* Default commands creation */
    int CreateDefaultCommands ();
    int RegisterNewCommand (CCommand *);
};

#endif /* _BOT_H */
