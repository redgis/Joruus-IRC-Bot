
CXXFLAGS= $(OPTIONS) $(INCLUDES) -fPIC
CFLAGS= $(OPTIONS) $(INCLUDES) -fPIC
LDFLAGS= $(LIBDIRS) $(LIBS) -D_REENTRANT -rdynamic

CC=g++
CXX=g++
AR=ar


OBJECTS=$(SRC:.cpp=.o)

all: $(TARGETS)

clean:
	rm -f $(OBJECTS) $(TARGETS)

cleanobj:
	rm -f $(OBJECTS)

depend: 
	makedepend $(INCLUDES) -Y -fMakefile $(SRC); rm Makefile.bak

again: clean $(TARGETS)
