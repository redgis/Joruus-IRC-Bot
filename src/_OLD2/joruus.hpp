/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _JORUUS_HPP
#define _JORUUS_HPP

#define _DEBUG_

//FIXME
//IRC protocol defines
#define IRC_COLOR ""
#define IRC_BOLD ""
#define IRC_UNDERLINE ""
#define IRC_CRLF "\x0D\x0A"
#define IRC_SPACE "\x20"
#define IRC_SEPARATORS "\x20\x0D\x0A\0"

#define CONSOLE_BLACK "\033[22;30m"
#define CONSOLE_RED "\033[22;31m"
#define CONSOLE_GREEN "\033[22;32m"
#define CONSOLE_BROWN "\033[22;33m"
#define CONSOLE_BLUE "\033[22;34m"
#define CONSOLE_MAGENTA "\033[22;35m"
#define CONSOLE_CYAN "\033[22;36m"
#define CONSOLE_GRAY "\033[22;37m"
#define CONSOLE_DGRAY "\033[01;30m"
#define CONSOLE_LRED "\033[01;31m"
#define CONSOLE_LGREEN "\033[01;32m"
#define CONSOLE_YELLOW "\033[01;33m"
#define CONSOLE_LBLUE "\033[01;34m"
#define CONSOLE_LMAGENTA "\033[01;35m"
#define CONSOLE_LCYAN "\033[01;36m"
#define CONSOLE_WHITE "\033[01;37m"
#define CONSOLE_RESET_COLOR "\033[0m"

/*
#define CONSOLE_BLACK ""
#define CONSOLE_RED ""
#define CONSOLE_GREEN ""
#define CONSOLE_BROWN ""
#define CONSOLE_BLUE ""
#define CONSOLE_MAGENTA ""
#define CONSOLE_CYAN ""
#define CONSOLE_GRAY ""
#define CONSOLE_DGRAY ""
#define CONSOLE_LRED ""
#define CONSOLE_LGREEN ""
#define CONSOLE_YELLOW ""
#define CONSOLE_LBLUE ""
#define CONSOLE_LMAGENTA ""
#define CONSOLE_LCYAN ""
#define CONSOLE_WHITE ""
#define CONSOLE_RESET_COLOR ""
*/

#define MSG_MAX_SIZE 512
#define BUFFER_MAX_SIZE 1024

//ANSI C++ includes
#include <iostream>
#include <string>

//STL C++ includes
#include <map>
#include <vector>
#include <algorithm>
#include <queue>

extern "C" {

//ANSI C includes
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctime>

//Linux Headers
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <argz.h>

//Linux/POSIX C includes
#include <semaphore.h>

//Local includes
//#include "stdoutstrings.h"
}

using namespace std;

namespace Joruus 
{
  /* Classes in the namespace */

  // Common
  class CThread;

  // Joruus
  class CBot;

  // IRC model
  class CUser;
  class CChannel;
  class CServer;
  class CChannel;
  class CIrcMessage;

  //Network
  class CConnexion;
  class CClientConnexion;
  class CClient_TCPConnexion;
  class CClient_UDPConnexion;
  class CServerConnexion;
  class CServer_TCPConnexion;
  class CServer_UDPConnexion;
  class CClient_IRCConnexion;

  //Events
  class CEventType;
  class CEventManager;
  class CEvent;

  //Commands
  class CCommand;
  
  /************* Types ***************/

  typedef enum eTrilean {FALSE = 0, TRUE = 1, UNKNOWN = -1} trilean;

  /* typedef for timestamp */
  typedef time_t TTimestamp;

  /* Function pointer to a handler */
  typedef int (*TEventHandler) (Joruus::CEvent * );
  typedef int (*TCommandHandler) (string & Arguments, CCommand * Command, int argc, char * argv[]);


  /**** Global Variables *****/

  /* Bot object */
  extern CBot * JoruusCBot;

  /* Event bindings map : keyword to EventType object */
  extern map <string, Joruus::CEventType *> EventTypeMap;

  /* Command bindings map : keyword to CCommand object */
  extern map <string, Joruus::CCommand *> CommandMap;
  

  /* Init various data */
  int init ();

  /* Parse a char * string for space separated arguments. returns the argument number and arguments in char * args[] */
  extern int parseArguments (char * szArguments, char * args[]);

  /* Output methods */
  extern void debugMessagePrint (const char * Message, char * inFile, int atLine, TTimestamp theTime = time (nullptr));
  extern void sysMessagePrint (const char * Message, char * inFile = __FILE__, int atLine = __LINE__, TTimestamp theTime = time (nullptr));
  extern void ircStatusPrint (const char * Message, char * serverName, TTimestamp theTime = time (nullptr));
  extern void ircMessagePrint (const char * Message, char * serverName, char * from, char * to, TTimestamp theTime = time (nullptr));
  extern void ircNoticePrint (const char * Message, char * serverName, char * from, char * to, TTimestamp theTime = time (nullptr));

  class JoruusException : public exception
  {
    private:
      const char * m_File;
      int m_Line;
      const char * m_Message;

    public:
      JoruusException (const char * Message, const char * file, int line) throw() : m_Message(Message), m_File(file), m_Line(line) {}
      virtual ~JoruusException () throw () {};

      void Show () throw() { Joruus::sysMessagePrint (m_Message, m_File, m_Line); }
  };
};

using namespace Joruus;

#endif /* _JORUUS_HPP */

