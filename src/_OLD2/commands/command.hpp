/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _COMMAND_H
#define _COMMAND_H

#include "../joruus.hpp"

class Joruus::CCommand
{
  private:
    string m_Command;     /* Command keyword */
    string m_Description; /* Command description */
    string m_UsageHelp;   /* Usage description of the command */
    int m_NbMinArguments; /* Number of arguments for this command, -1 if undefined number */
    int m_NbMaxArguments;   

    /* List of handlers for this command. */
    vector <Joruus::TCommandHandler> m_Handlers;
    
  public:
    /* Constructor and destructor */
    CCommand (const char * cmd, const char * desc = "", const char * use = "No help available.", int nbminarg = 0, int nbmaxarg = 0);
    virtual ~CCommand ();
    
    /* Manage handlers for this type of event */
    void ClearHandlers ();
    void AddHandler (Joruus::TCommandHandler);
    
    /* Get accessors */
    string & getCommand ();
    string & getDescription ();
    string & getUsageHelp ();
    int getNbMinArguments ();
    int getNbMaxArguments ();

    /* Will call all the handlers for this command */
    void RunHandlers (string & arguments);
    
};

#endif /* _COMMAND_H */
