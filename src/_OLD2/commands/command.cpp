/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "command.hpp"

/***************************************************************************/
Joruus::CCommand::CCommand(const char * cmd, const char * desc, const char * use, int minargs, int maxargs)
{
  m_Command = string(cmd);
  m_Description = string(desc);
  m_UsageHelp = string(use);
  m_NbMinArguments = minargs;
  m_NbMaxArguments = maxargs;

  ClearHandlers ();
}

/***************************************************************************/
Joruus::CCommand::~CCommand ()
{
  /* remove all handlers */
  ClearHandlers ();
}

/***************************************************************************/
void Joruus::CCommand::ClearHandlers ()
{
  /* remove all handlers */
  m_Handlers.clear ();
}

/***************************************************************************/
void Joruus::CCommand::AddHandler (Joruus::TCommandHandler newHdl)
{
  m_Handlers.push_back (newHdl);
}

/***************************************************************************/
void Joruus::CCommand::RunHandlers (string & Arguments)
{
  /***********/
  //debugMessagePrint ((string(" CMD /") + m_Command + " " + Arguments).c_str (), __FILE__, __LINE__);
  /***********/

  char * argv[256];
  char szArguments[Arguments.size()+1];
  int nbargs;

  /* Verify arguments (m_NbArguments = -1 means unspecified arg number => nothing to check) */
  // Get arguments into char *
  strcpy (szArguments, Arguments.c_str());
  nbargs = Joruus::parseArguments (szArguments, argv);
  
  if ( ((m_NbMinArguments != -1) && (nbargs < m_NbMinArguments))
    || ((m_NbMaxArguments != -1) && (nbargs > m_NbMaxArguments)) )
  {
    
    sysMessagePrint ((string (m_Command) + " - Illegal number of parameters.\n" + m_UsageHelp).c_str (), (char *) __FILE__, __LINE__);
    return;
  }

  /* run all handlers */
  vector <Joruus::TCommandHandler>::iterator itCurrentHandler = m_Handlers.begin ();
  vector <Joruus::TCommandHandler>::iterator itLastHandler = m_Handlers.end ();
  
  while (itCurrentHandler != itLastHandler)
  {
    (*itCurrentHandler) (Arguments, this, nbargs, argv);
    itCurrentHandler++;
  }
  

  return;
}

/***************************************************************************/
string & Joruus::CCommand::getCommand ()
{
  return m_Command;
}

/***************************************************************************/
string & Joruus::CCommand::getDescription ()
{
  return m_Description;
}

/***************************************************************************/
string & Joruus::CCommand::getUsageHelp ()
{
  return m_UsageHelp;
}

/***************************************************************************/
int Joruus::CCommand::getNbMinArguments ()
{
  return m_NbMinArguments;
}

/***************************************************************************/
int Joruus::CCommand::getNbMaxArguments ()
{
  return m_NbMaxArguments;
}
