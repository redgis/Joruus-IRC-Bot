/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "bot.hpp"
#include "events/eventmanger.hpp"
#include "network/tcpclientconnection.hpp"

/***************************************************************************/
Joruus::CBot::CBot ()
{
  // Load user data.
  m_IrcConnexions.clear ();
  m_CurrentServer = nullptr;

  /* Switch console to non blocking mode */
  fcntl (0, F_SETFL, O_NONBLOCK | fcntl (0, F_GETFL));

  // Initialize console commands
  CreateDefaultCommands ();

  // Start EventManager;
  m_EventManager->CreateDefaultEventTypes ();
}

/***************************************************************************/
Joruus::CBot::~CBot ()
{
  /* delete the event manager */
  delete EventManager;

  /* remove and delete servers */
  vector <CClient_IRCConnexion *>::iterator currentConnection = m_IrcConnexions.begin ();
  vector <CClient_IRCConnexion *>::iterator lastConnection = m_IrcConnexions.end ();

  while (currentConnection != lastConnection)
  {
    delete (*currentConnection);
    currentConnection ++;
  }
  
  m_IrcConnexions.clear ();

}

/***************************************************************************/
int Joruus::CBot::FillFdSet ()
{
  int Sock;
  int HighestFd = 0;
  
  /* Initialise descriptors to be survey by Select */
  FD_ZERO (&m_ReadFd);
  FD_ZERO (&m_ErrorFd);

  FD_SET (0, &m_ReadFd);
  FD_SET (0, &m_ErrorFd);
  
  vector <Joruus::CClient_IRCConnexion *>::iterator itCurrentConnection = m_IrcConnexions.begin ();
  vector <Joruus::CClient_IRCConnexion *>::iterator itLastConnection = m_IrcConnexions.end ();

  while (itCurrentConnection != itLastConnection)
  {
    Sock = (*itCurrentConnection)->getSocket ();

    if (Sock != -1)
    {
      FD_SET (Sock, &m_ReadFd);
      FD_SET (Sock, &m_ErrorFd);
      HighestFd = (Sock > HighestFd)?Sock:HighestFd;
    }

    itCurrentConnection ++;
  }
  return HighestFd + 1;
}

/***************************************************************************/
/* Main loop with watchdog on the sockets and standard input */
void Joruus::CBot::run ()
{
  Joruus::CEvent * Event;
  Joruus::CIrcMessage Message;
  string TmpMessage;
  
  int Sock;
  int HighestFd;
  int mutexResult;
  
  //FIXME
  bool Exit = false;
  int Res;
  unsigned char IncomingData[BUFFER_MAX_SIZE];

  struct timeval Timeout;
  //struct timespec Timeout;

  vector <Joruus::CClient_IRCConnexion *>::iterator itCurrentServer;
  vector <Joruus::CClient_IRCConnexion *>::iterator itLastServer;

  while (!Exit)
  {
    /* Reinitialize Timeout */
    Timeout.tv_sec = 0;
    Timeout.tv_usec = 250000;
    //Timeout.tv_nsec = 250000000;


    /* Initialize fd_set */
    HighestFd = FillFdSet ();
    
    /* Listen to sockets (select) //FD_SETSIZE */
    Res = select (HighestFd, (fd_set *) &m_ReadFd, (fd_set *) nullptr, (fd_set *) &m_ErrorFd, (struct timeval *) &Timeout);
    //Res = pselect (HighestFd, (fd_set *) &m_ReadFd, (fd_set *) nullptr, (fd_set *) &m_ErrorFd, (struct timespec *) &Timeout, nullptr);

    if (Res == -1)
      sysMessagePrint ((const char *) strerror(errno), (char *)__FILE__, __LINE__); //select error
    else
    {
      /* If StdIn modified: process new data */
      if (FD_ISSET (0, &m_ReadFd) or true)
      {
        while ((Res = read (0, IncomingData, BUFFER_MAX_SIZE)) > 0)
        {
          IncomingData[Res] = '\0';

          /* Dispatch the message */
          if (Res != 0)
          {
            Message.Origine = 0;
            Message.Server = nullptr;
            m_Buffers[0] += string ((const char *) IncomingData);
            TmpMessage = m_Buffers[0];
            
            /* Parse each line of the data recieved */
            while (TmpMessage.find_first_of(IRC_CRLF) != string::npos)
            {
              Message.Content = TmpMessage.substr (0, TmpMessage.find_first_of(IRC_CRLF));

              if (TmpMessage.find_first_not_of(IRC_CRLF, TmpMessage.find_first_of(IRC_CRLF)) != string::npos )
                TmpMessage = TmpMessage.substr (TmpMessage.find_first_not_of(IRC_CRLF, TmpMessage.find_first_of(IRC_SEPARATORS))); // FIXME : pas sur que ca plante pas ca ...
              else
                TmpMessage = "";

              Event = parseMessage (Message);
            }
            m_Buffers[0] = TmpMessage;
          }
        }

        //if (Res == -1)
        //  sysMessagePrint ((const char *) strerror(errno), (char *) __FILE__, __LINE__); //error in reading stdin
      }

      // Console lost (closed ?)
      if (FD_ISSET (0, &m_ErrorFd))
      {
      }

      /* If a socket has been modified: process message from server */
      itCurrentServer = m_IrcConnexions.begin ();
      itLastServer = m_IrcConnexions.end ();


      while (itCurrentServer != m_IrcConnexions.end ())
      {
        Sock = (*itCurrentServer)->getSocket ();

        if (Sock != -1)
        {
          // Data arrived from socket
          if (FD_ISSET (Sock, &m_ReadFd) or true)
          {
            /* Read message recieved */
            while ((Res = recv (Sock, IncomingData, BUFFER_MAX_SIZE, 0)) > 0)
            {
              IncomingData[Res] = '\0';
              //cout << IncomingData << "\\" << endl;

              /* Dispatch the message if it comes from a known client (otherwise, ignore it) */
              if (Res != 0)
              {
                Message.Origine = Sock;
                Message.Server = (*itCurrentServer)->m_Server;

                m_Buffers[Sock] += string ((const char *) IncomingData);
                TmpMessage = m_Buffers[Sock];

                while (TmpMessage.find(IRC_CRLF) != string::npos)
                {
                  Message.Content = TmpMessage.substr (0, TmpMessage.find(IRC_CRLF));

                  TmpMessage = TmpMessage.substr (TmpMessage.find(IRC_CRLF) + strlen(IRC_CRLF));

                  //cout << "\"" << Message.Content << "\"" << endl;
                  Event = parseMessage (Message);
                }
                m_Buffers[Sock] = TmpMessage;
              }
            } /* while */

            //if (Res == -1)
            //  debugMessagePrint (strerror(errno), (char *) __FILE__, __LINE__);
          }

          // Socket likely to have gotten closed
          if (FD_ISSET (Sock, &m_ErrorFd))
          {
          }
        }

        itCurrentServer ++;
      }
    }

  } /* while */
}


/***************************************************************************/
CEvent * Joruus::CBot::parseMessage (Joruus::CIrcMessage & Message)
{
  Joruus::CEvent * EvtTmp = nullptr;
  
  string Prefix = "";
  string Nick = "";
  string UserName = "";
  string Domain = "";
  string Command = "";
  string Parameters = "";
  string TempString = "";
  
  //FIXME : a completer
  if (Message.Origine == 0) // Comes from the console
  {
    if (Message.Content[0] == '/') // A console command
    {
      // Execute command

      Command = Message.Content.substr (1, Message.Content.find_first_of (IRC_SEPARATORS) - 1);
      if (Message.Content.find_first_of (IRC_SEPARATORS) != string::npos)
        Parameters = Message.Content.substr (Message.Content.find_first_of (IRC_SEPARATORS) + 1);
        
      if (CommandMap.find(Command) != CommandMap.end ())
      {
        CommandMap[Command]->RunHandlers (Parameters);
      }
      else
      {
        if (m_CurrentServer != nullptr)
        {
          EvtTmp = new CEvent;

          EvtTmp->EventType = EventTypeMap[string("UNKNOWN_CMD")];

          EvtTmp->Incoming = false;
          EvtTmp->Server = m_CurrentServer->m_Server;
          EvtTmp->User = this;
          EvtTmp->Nick = Nick;
          EvtTmp->UserName = UserName;
          EvtTmp->Domain = Domain;
          EvtTmp->Keyword = Command;
          EvtTmp->Data = Parameters;
          EvtTmp->Timestamp = time (nullptr);

          m_EventManager->PostEvent (EvtTmp);
        }
      }
    }
    else // Message to current irc channel / interlocutor
    {
      if (m_CurrentServer != nullptr)
      {
        EvtTmp = new Joruus::CEvent;

        if (EventTypeMap.find(string("CONSOLE_MSG")) != EventTypeMap.end ())
          EvtTmp->EventType = EventTypeMap[string("CONSOLE_MSG")];
        else
          EvtTmp->EventType = EventTypeMap[string("UNKNOWN")];

        EvtTmp->Incoming = false;
        EvtTmp->Server = m_CurrentServer->m_Server;
        EvtTmp->User = this;
        EvtTmp->Nick = Nick;
        EvtTmp->UserName = UserName;
        EvtTmp->Domain = Domain;
        EvtTmp->Keyword = EvtTmp->EventType->getKeyword ();
        EvtTmp->Data = Message.Content;
        EvtTmp->Timestamp = time (nullptr);

        m_EventManager->PostEvent (EvtTmp);
      }
    }
  }
  else /**********************************************************************/
  {
    /*
    Les serveurs et les clients IRC g\'en\`erent chacun des messages pouvant ou non g\'en\'erer une r\'eponse. Ces
    messages ont toujours le m\^eme format (les crochets indiquent une partie optionnelle dans le message).
    
    [{:}{prefix}{SP}]{COMMANDE}[{SP}{PARAMETRES}]{CRLF}
    
    Le s\'eparateur SP est un espace (code ascii : 0x20). Le s\'eparateur CRLF est le duo "Retour Chariot, Fin de Ligne"
    (codes ascii 0x0D et 0x0A).

    \`A noter que les caract\`eres CR (0x0D), LF (0x0A) et NUL (0x00) sont interdits dans les messages IRC. De plus, la
    longueur d'un message IRC est de 512 caract\`eres au maximum, y compris le CRLF final.

    Le pr\'efix n'est utilis\'e que pour un message provenant d'un serveur, il doit \^etre ignor\'e s'il provient d'un client. Si le
    message est destin\'e \`a un serveur ou que le message a \'et\'e \'emis sur le r\'eseau par un serveur, le pr\'efixe est
    simplement le nick du client, ou le nom du serveur ayant envoy\'e le message sur le r\'eseau. Si le message est
    destin\'e \`a un client et que c'est un client qui a envoy\'e le message sur le r\'eseau, alors le pr\'efix suit cette syntaxe :
    
    {NICK}{!}{NOM DU CLIENT}{@}{DOMAINE DU CLIENT}

    La commande est soit un mot (une suite de une ou plusieurs lettres (A-Z) insensibles \`a la casse), soit un code de
    trois chiffres. Les codes de trois chiffres correspondent \`a des r\'eponses de serveur, chacun ayant un nom, pour
    chaque commande, les r\'eponses possibles seront list\'ees. Les param\`etres sont des cha\^\i{}nes de caract\`eres sans
    espace, s\'epar\'ees par des espaces. \`A noter que le dernier param\`etre peut contenir des espaces, dans ce cas, son
    premier caract\`ere doit alors \^etre les deux-points (:). Chaque commande a ses param\`etres.
    */

    debugMessagePrint (Message.Content.c_str(), (char *) __FILE__, __LINE__);

    // comes from a server

    // Find prefix
    if (Message.Content[0] == ':')
    {
      Prefix = Message.Content.substr (1, Message.Content.find_first_of (IRC_SPACE) - 1);
      Message.Content = Message.Content.substr (Message.Content.find_first_of (IRC_SPACE) + 1);
      
      //Parse the prefix
      size_t PointIndex = Prefix.find_first_of ('!');
      size_t AtIndex = Prefix.find_first_of ('@');

      if ( PointIndex != string::npos)
        Nick = Prefix.substr (0, PointIndex);
      
      if ( (AtIndex != string::npos) && (PointIndex != string::npos) )
        UserName = Prefix.substr (PointIndex+1, AtIndex - PointIndex);
      else
        Domain = Prefix;

      if (AtIndex != string::npos)
        Domain = Prefix.substr(AtIndex+1);
    }
    else
    {
      Prefix = "";
    }
    
    // Parse the reste of the command
    Command = Message.Content.substr (0, Message.Content.find_first_of (IRC_SEPARATORS));
    Parameters = Message.Content.substr (Message.Content.find_first_not_of (IRC_SEPARATORS, Message.Content.find_first_of (" \n\r\b")));
    
    //cout << "\"" << Command << "\" \"" << Parameters << "\"" << endl;
    
    EvtTmp = new Joruus::CEvent;

    if (EventTypeMap.find(Command) != EventTypeMap.end ())
      EvtTmp->EventType = EventTypeMap[Command];
    else
      EvtTmp->EventType = EventTypeMap[string("UNKNOWN")];

    EvtTmp->Incoming = true;
    EvtTmp->Server = Message.Server;
    EvtTmp->User = Message.Server->getUserByName(UserName); // FIXME: should I create user if not created ? or let events handlers take care of it ?
    EvtTmp->Nick = Nick;
    EvtTmp->UserName = UserName; 
    EvtTmp->Domain = Domain;
    EvtTmp->Keyword = Command;
    EvtTmp->Data = Parameters;
    EvtTmp->Timestamp = time (nullptr);

    m_EventManager->PostEvent (EvtTmp);

  }
  return EvtTmp;
}

/***************************************************************************/
int Joruus::CBot::CreateDefaultCommands ()
{
  CCommand * TmpCmd;
  int CommandIndex = 0;

  while (Command[CommandIndex][0][0] != '\0')
  {
    TmpCmd = new CCommand (Command[CommandIndex][0], Command[CommandIndex][1], Command[CommandIndex][2], CommandNbArgs[CommandIndex][0],CommandNbArgs[CommandIndex][1]);
    TmpCmd->AddHandler (CommandDefaultHandler[CommandIndex]);
    RegisterNewCommand (TmpCmd);
    CommandIndex ++;
  }
  
  return 0;
}

/***************************************************************************/
int Joruus::CBot::RegisterNewCommand (CCommand * newCommand)
{
  /* register the new type */
  Joruus::CommandMap[newCommand->getCommand()] = newCommand;
  
  return Joruus::CommandMap.size ();
}

/***************************************************************************/
CClient_IRCConnexion * Joruus::CBot::getCurrentServer ()
{
  return m_CurrentServer;
}

/***************************************************************************/
void Joruus::CBot::setCurrentServer (CClient_IRCConnexion * newServ)
{
  m_CurrentServer = newServ;
}

/***************************************************************************/
void Joruus::CBot::setCurrentServer (int serverID)
{
  if ( (serverID >= 0) && (serverID < (int) m_IrcConnexions.size ()) )
    m_CurrentServer = m_IrcConnexions[serverID];
}
