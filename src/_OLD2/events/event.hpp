/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot 
 */

#ifndef EVENT_HPP_
#define EVENT_HPP_

#include "../joruus.hpp"

/****************************************************************************/
class Joruus::CEvent
{
public:
  CEvent();
  virtual
  ~CEvent();

  CEventType * EventType;
  Joruus::TTimestamp Timestamp; // When the event occured
  CServer * Server; // Server that sent the message or we are sending the message to (useless ?) FIXME
  CUser * User; // User that sent the message FIXME: useless ?
  string Nick;  // User nickname (found in message prefix)
  string UserName;  // User name (found in message prefix)
  string Domain;  // User or server domain (found in message prefix)
  bool Incoming;  // Incomming or outgoing message ?
  string Keyword; // Command recieved
  string Data;    // Parameters of additional data

};

#endif /* EVENT_HPP_ */
