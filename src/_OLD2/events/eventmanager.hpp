/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _EVENTMANAGER_H
#define _EVENTMANAGER_H

#include "../joruus.hpp"

class Joruus::CEventManager
{
  private:
    /* Event queue */
    queue <Joruus::CEvent *> m_EventQueue;
    
    //FIXME : devrait etre donnee membre de CThread ?
    bool m_Exit;

  public:
    /* Constructor and destructor */
    CEventManager ();
    virtual ~CEventManager ();

    /* Register an new event class with its ID (=> EventTypeMap) */
    int RegisterNewEventType (Joruus::CEventType * newEventType);
    /* Create default predefined events */
    int CreateDefaultEventTypes ();

    /* Post a new event */
    void PostEvent (Joruus::CEvent * newEvent);
    /* Treat the next event */
    void TreatEvent ();
    /* Do the event queue treatments */
    virtual void run ();
};

#endif /* _EVENTMANAGER_H */
