/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#ifndef _EVENTTYPE_H
#define _EVENTTYPE_H

#include "../joruus.hpp"


class Joruus::CEventType
{
  private:
    string m_Keyword;
    string m_Description;     /* Description of this type of event */
    
    /* List of handlers for this event. */
    vector <Joruus::TEventHandler> m_Handlers;
    
  public:
    /* Constructor and destructor */
    CEventType (string Keyword, string Description = "");
    virtual ~CEventType ();
    
    /* Manage handlers for this type of event */
    void ClearHandlers ();
    void AddHandler (Joruus::TEventHandler);

    /* Access methods */
    string & getKeyword ();
    string & getDescription ();

    /* Will call all the handlers for this event type */
    void RunHandlers (Joruus::TEvent *);
};

#endif /* _EVENTTYPE_H */
