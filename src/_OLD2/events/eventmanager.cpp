/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "eventmanager.hpp"
#include "eventtype.hpp"
#include "default_event_handlers.hpp"

/***************************************************************************/
Joruus::CEventManager::CEventManager ()
{
  m_Exit = false;
}

/***************************************************************************/
Joruus::CEventManager::~CEventManager ()
{
  m_Exit = true;

  /* remove all remaining events */
  while ( !m_EventQueue.empty() )
  {
    delete m_EventQueue.front();
    m_EventQueue.pop ();
  }
}

/***************************************************************************/
int Joruus::CEventManager::RegisterNewEventType (Joruus::CEventType * newEventType)
{
  /* register the new type */
  Joruus::EventTypeMap[newEventType->getKeyword()] = newEventType;
  
  return Joruus::EventTypeMap.size ();
}

/***************************************************************************/
int Joruus::CEventManager::CreateDefaultEventTypes ()
{
  Joruus::CEventType * eventType;
  //CEventType (string & Keyword, string & Description = "") : m_Keyword(Keyword),m_Description(Description);
  
  int EventIndex = 0;

  while (strcmp(EventTypeKeywordsAndDescription[EventIndex][0], "\0"))
  {
    /* Create a new event type */
    eventType = new CEventType(EventTypeKeywordsAndDescription[EventIndex][0], EventTypeKeywordsAndDescription[EventIndex][1]);

    /* Create the associated default handler */
    Joruus::TEventHandler defaultHandler = EventTypeDefaultHandler[EventIndex];

    /* Set the default handler for this event */
    eventType->AddHandler (defaultHandler);
    
    /* Register this new event class in the event manager */
    EventManager->RegisterNewEventType (eventType);
    
    EventIndex++;
  }
  
  return 0;

}

/***************************************************************************/
void Joruus::CEventManager::PostEvent (Joruus::TEvent * newEvent)
{
  m_EventQueue.push (newEvent);
}

/***************************************************************************/
void Joruus::CEventManager::TreatEvent ()
{
  Joruus::TEvent * evt = m_EventQueue.front ();

  /***********/
  // debugMessagePrint ((evt->EventType->getKeyword () + " " + evt->Nick + "@" + evt->Domain + " : " + evt->Keyword + " " + evt->Data).c_str (), __FILE__, __LINE__);
  /***********/

  /* Run associated handlers */
  evt->EventType->RunHandlers(evt);
  //delete evt;
  m_EventQueue.pop();
}

/***************************************************************************/
void Joruus::CEventManager::run ()
{
  int result;

  while (!m_Exit)
  {
    result = pthread_mutex_lock (&sem_EventMgr);
    if (result != 0)
      debugMessagePrint (strerror(result), (char *) __FILE__, __LINE__);

    while (!m_EventQueue.empty())
    {
      TreatEvent();
    }

    result = pthread_mutex_unlock (&sem_EventMgr);
    if (result != 0)
      debugMessagePrint (strerror(result), (char *) __FILE__, __LINE__);
  }
}

