/*
 * Regis Martinez - regis.martinez@worldonline.fr
 * JoruusCBot - C++ IRC Bot
 */

#include "eventtype.hpp"

/***************************************************************************/
Joruus::CEventType::CEventType (string Keyword, string Description) : m_Keyword(Keyword),m_Description(Description)
{
  m_Handlers.clear ();
}

/***************************************************************************/
Joruus::CEventType::~CEventType ()
{
  /* remove all handlers */
  ClearHandlers ();
}

/***************************************************************************/
void Joruus::CEventType::ClearHandlers ()
{
  /* remove all handlers */
  m_Handlers.clear ();
}

/***************************************************************************/
void Joruus::CEventType::AddHandler (Joruus::TEventHandler newHdl)
{
  m_Handlers.push_back (newHdl);
}

/***************************************************************************/
string & Joruus::CEventType::getKeyword ()
{
  return m_Keyword;
}

/***************************************************************************/
string & Joruus::CEventType::getDescription ()
{
  return m_Description;
}

/***************************************************************************/
void Joruus::CEventType::RunHandlers (Joruus::TEvent * evt)
{
  /* run all handlers */
  vector <Joruus::TEventHandler>::iterator itCurrentHandler = m_Handlers.begin ();
  vector <Joruus::TEventHandler>::iterator itLastHandler = m_Handlers.end ();
  
  while (itCurrentHandler != itLastHandler)
  {
    (*itCurrentHandler) (evt);
    itCurrentHandler++;
  }

}
