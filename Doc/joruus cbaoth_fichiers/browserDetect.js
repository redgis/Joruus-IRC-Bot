function Is()
{
	this.subscriber = false;
	var agent = navigator.userAgent.toLowerCase();
	this.major = parseInt(navigator.appVersion);
	this.minor = parseFloat(navigator.appVersion);
	this.ns  = ((agent.indexOf('mozilla')!=-1) && ((agent.indexOf('spoofer')==-1) && (agent.indexOf('compatible') == -1)));
	this.ie   = (agent.indexOf("msie") != -1);
	this.win   = (agent.indexOf("win")!=-1);
	this.mac   = (agent.indexOf("mac")!=-1);

	this.ie5 = (agent.indexOf("msie 5")!=-1);
	this.ie6 = (agent.indexOf("msie 6")!=-1);
	this.ns4 = (this.ns && (this.major <= 4));
	this.ns6 = (agent.indexOf("netscape6")!=-1);
	this.ns7 = (agent.indexOf("netscape/7")!=-1);

	this.safari = (agent.indexOf("safari")!=-1);
	this.firefox = (agent.indexOf("firefox")!=-1);

	this.opera = (agent.indexOf("opera")!=-1);
	this.opera7 = (agent.indexOf("opera 7")!=-1);

	var VisitorID =  document.cookie.indexOf("VisitorID" + "=");
	var Skakoan =  document.cookie.indexOf("Skakoan" + "=");

	if (VisitorID != -1) {
	if(Skakoan !=-1)
		this.subscriber = true;
	}
	else {
		this.subscriber = false;
	}

}

var is = new Is();

var teaserPrefix = "";

if(!is.subscriber) {
	teaserPrefix = "/hyperspace/tease.html?tPage=";
}

// remove IE association with opera
if(is.opera) {
	is.ie = false;
	is.ie5 = false;
	is.ie6 = false;
}


function hsTease(theURL) {
	window.location=teaserPrefix+theURL;
}




function popExpanded(world, img, caption, width, height, useScrollBars, useImport) {
	popExpand = null;
	if(popExpand == null || popExpand.closed) {
		if(useScrollBars == "yes") {
			useScrollBars  = 'yes';
		} else {
			useScrollBars  = 'no';
		}


		var url = "/meta/rd/expand/expand.html?world="+world+"&img="+img+"&width="+width+"&height="+height+"&import="+useImport;

		if(typeof(caption) != "undefined") {	url += "&caption="+escape(caption); }

		if(typeof(useImport) == "undefined" || useImport != "true") {
			popExpand = window.open(url,"popExpand","width=1,height=1,left=1,top=1,toolbar=no,resizable=no,scrollbars="+useScrollBars+",location=no,directories=no,menubar=no");
		} else {
			var screenWidth		=	(screen.width - width) / 2;
			var screenHeight	=	(screen.height - height) / 2;

			popExpand = window.open(url,"popExpandImport","width="+width+",height="+height+",left="+screenWidth+",top="+screenHeight+",toolbar=no,resizable=no,scrollbars="+useScrollBars+",location=no,directories=no,menubar=no");
		}
	}
}

var newwin;
function doVideo(url)
{

	var screenWidth		=	(screen.width - 300) / 2;
	var screenHeight	=	(screen.height - 300) / 2;


	var winWidth		=	915;
	var winHeight		=	650;

	if(screen.width == 800)
	{
		winWidth  = 790;
		winHeight = 550;
	}


	newwin = window.open(url,"vidPop","width="+winWidth+",height="+winHeight+",toolbar=no,resizable=no,scrollbars=no,location=no,directories=no,menubar=no,top=10,left=10");


	newwin.focus
}


function getCookie(name)
{
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1)
    {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1)
    {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

function setCookie(name, value) {
	var today = new Date();
	var expiry = new Date(today.getTime() + 1 * 1 * 60 * 60 * 1000); // plus 1 hour
  if (value != null && value != "")
	document.cookie=name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
  	shopAd = document.cookie; // update shopAd
}



function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
	var pair = vars[i].split("=");
	if (pair[0] == variable) {
	  return pair[1];
	}
  }
}


function popVideo(world, url, movieWidth, movieHeight, windowWidth, windowHeight, autoplay)
{

	if(windowWidth == "undefined") {
		windowWidth = movieWidth;
	}
	if(windowHeight == "undefined") {
		windowHeight = movieHeight;
	}

	if(typeof(autoplay) == "undefined") {
		autoplay = "true";
	}

	// add 15 to height for player ui
	movieHeight += 15;

	var screenWidth		=	(screen.width - windowWidth) / 2;
	var screenHeight	=	(screen.height - windowHeight) / 2;

	winUrl = "/meta/rd/expand/video.html?world="+world+"&url="+url+"&mw="+movieWidth+"&mh="+movieHeight+"&ww="+windowWidth+"&wh="+windowHeight+"&ap="+autoplay;

	vidWin = window.open(winUrl,"vidPopRd","width="+windowWidth+",height="+windowHeight+",status=yes,toolbar=no,resizable=no,scrollbars=no,location=no,directories=no,menubar=no,top="+screenWidth+",left="+screenHeight);
}

var audiocastWindow = null;
function popAudioCast(url, fromHS)
{
	urlSplit = url.split('/');
	urlID = urlSplit[urlSplit.length-1].toString().split('.');

	// urlID[0]

	if(fromHS == true || typeof(__onAudioCast) != "undefined") {
		windowWidth = 446;
		windowHeight = 499;

		var screenWidth		=	(screen.width - windowWidth) / 2;
		var screenHeight	=	(screen.height - windowHeight) / 2;

		newUrl = "/meta/rd/audiocast/audiocast.html?url="+url
		audiocastWindow = window.open(newUrl,"audiocastWindow","width="+windowWidth+",height="+windowHeight+",left="+screenWidth+",top="+screenHeight +",resizable=no,scrollbars=no,toolbar=no,location=no,directories=no,menubar=no");
		setCookie("audio", 'null');
		audiocastWindow.focus();
	} else {
		setCookie("audio", url);
		window.location = "/hyperspace/member/audiocast/?url="+url;
	}
}


var webCamWindow = null;
function popWebCamMovie(url, fromHS) {
	if(fromHS == true) {
		windowWidth = 750;
		windowHeight = 740;

		var screenWidth		=	(screen.width - windowWidth) / 2;
		var screenHeight	=	(screen.height - windowHeight) / 2;

		if(url != null) {
			var urlSplit = url.split('.');
			if(urlSplit.length > 1) {
				var newUrl = "/meta/rd/webcammovies/webcammovies.html?wcmurl="+url;
			} else {
				var newUrl = "/meta/rd/webcammovies/webcammovies.html?wcmurl=/hyperspace/member/wcm/wcm"+url+".xml";
			}
			webCamWindow = window.open(newUrl,"webcamWindow","width="+windowWidth+",height="+windowHeight+",left="+screenWidth+",top="+screenHeight +",resizable=no,scrollbars=no,toolbar=no,location=no,directories=no,menubar=no");
			webCamWindow.focus();
		}
	} else {
		setCookie("wcm", url);
		window.location = "/hyperspace/member/wcm/?wcmurl="+url;
	}

}

var qaPopup = null;
function popSWQA(url)
{
	windowWidth = 791;
	windowHeight = 605;

	var screenWidth		=	(screen.width - windowWidth) / 2;
	var screenHeight	=	(screen.height - windowHeight) / 2;

	var qaUrl = url;
	qaPopup = window.open(qaUrl,"qaPopup","width="+windowWidth+",height="+windowHeight+",left="+screenWidth+",top="+screenHeight +",resizable=no,scrollbars=no,toolbar=no,location=no,directories=no,menubar=no");
	qaPopup.focus();
}

