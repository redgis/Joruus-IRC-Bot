var loggedIn = false;
allcookies = document.cookie;
var cookieName = "ScreenName=";
pos = allcookies.indexOf(cookieName);
var startValuePos = pos + cookieName.length;
var endValuePos   = allcookies.indexOf(";", startValuePos);

if (endValuePos == -1) endValuePos = allcookies.length;
    var value = allcookies.substring(startValuePos, endValuePos);
    value = unescape(value);
    
	if(pos == -1) {
		loggedIn = false
		logUrl = "/webapps/registration/sign-out";
	}
	else {
		loggedIn = true;
		logUrl = "/webapps/registration/sign-in";
	}

function showServicesText(locationFlag, path) {
	if(typeof(path) == "undefined") {
		path = "/";
	}
	if(loggedIn) {
		switch(locationFlag) {
			case "topPage":
				document.write('<a href="/webapps/registration/sign-out" class="serviceText">sign off</a> | <a href="/webapps/registration/update-registration" class="serviceText">your settings</a> | <a href="http://cargobay.starwars.com/webapps/cargobay/fan-collection?type=want" class="serviceText">your collection</a> | <a href="/site/faq.html" class="serviceText">site help</a>');
				break;
			default:
				break;
		}
	}
	else {
		switch(locationFlag) {
			case "topPage":
				document.write('<a href="/webapps/registration/sign-in?forward='+path+'" class="serviceText">sign on</a> | <a href="/site/faq.html" class="serviceText">site help</a>');
				break;
			default:
				break;
		}
	}
}

function showUserName() {

	
	if(loggedIn) {
		document.write('Welcome '+value);
	}
	else {
		document.write('Hello, you are not signed on.');
	}
}