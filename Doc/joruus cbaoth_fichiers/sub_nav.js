var currentMenu = null;
var subNavArray =  new Array("updatesDrop", "categoryDrop", "appearanceDrop");
var showPop = false;

function initSubNav() {
	for(var x=0; x<subNavArray.length; x++) {
		adjustSubPos(subNavArray[x]);
	}
	showPop = true;
}

function adjustSubPos(menuToShow) {
	var xTop = 23;
	var xLeft = 0;
	
	var mySubNavElement = document.getElementById("subNavigation");
	var mySubMenuElement = document.getElementById(menuToShow);
	
	switch(menuToShow) {
		case "updatesDrop":
			xLeft = -10;
			break;
		case "categoryDrop":
			xLeft = 58;
			break;
		case "appearanceDrop":
			xLeft = 154;
			break;
		default:
			break;
	}
	
	xTop  += parseInt(mySubNavElement.offsetTop);
	xLeft += parseInt(mySubNavElement.offsetLeft);

	mySubMenuElement.style.top  = xTop  + "px";
	mySubMenuElement.style.left = xLeft + "px";
}

function showSubMenu(menuToShow) {
	if(showPop) {
		if(menuToShow != currentMenu && currentMenu != null) {
			hideSubMenu();
		}
		document.getElementById(menuToShow).style.visibility = "visible";
		currentMenu = menuToShow;
	}
}

function hideSubMenu() {
	document.getElementById(currentMenu).style.visibility = "hidden";
}

function changeSubBG(subNavItem) {
	
	document.getElementById(subNavItem+"_bullet").style.backgroundColor = "#D7DDDD";
	document.getElementById(subNavItem).style.backgroundColor = "#D7DDDD";
	
		switch(subNavItem) {
			case "updates_link_1":
				document[subNavItem+'_bottom'].src=eval("updates_bottom_high.src");
			break;
			case "category_link_9":
				document[subNavItem+'_bottom'].src=eval("category_bottom_high.src");
			break;
			case "appearance_link_8":
				document[subNavItem+'_bottom'].src=eval("appearance_bottom_high.src");
			break;
		default:
			break;
		}
} 
function removeSubBG(subNavItem, bottomItem) {
	document.getElementById(subNavItem+"_bullet").style.backgroundColor = "";
	document.getElementById(subNavItem).style.backgroundColor = "";

		switch(subNavItem) {
			case "updates_link_1":
				document[subNavItem+'_bottom'].src=eval("updates_bottom.src");
				break;
			case "category_link_9":
				document[subNavItem+'_bottom'].src=eval("category_bottom.src");
				break;
			case "appearance_link_8":
				document[subNavItem+'_bottom'].src=eval("appearance_bottom.src");
				break;
			default:
				break;
		}
}

function swapImgOn(img) {
		document[img].src = eval(img+"_on.src");
}

function swapImgOff(img) {
	document[img].src = eval(img+"_off.src");
}

var updates_off = new Image();
updates_off.src = "/databank/img/nav/updates_off.gif";
var updates_on = new Image();
updates_on.src = "/databank/img/nav/updates_on.gif";

var category_off = new Image();
category_off.src = "/databank/img/nav/category_off.gif";
var category_on = new Image();
category_on.src = "/databank/img/nav/category_on.gif";

var appearance_off = new Image();
appearance_off.src = "/databank/img/nav/appearance_off.gif";
var appearance_on = new Image();
appearance_on.src = "/databank/img/nav/appearance_on.gif";

var updates_bottom_high = new Image();
updates_bottom_high.src = "/databank/img/nav/bottom_updates_high.gif";
var updates_bottom = new Image();
updates_bottom.src = "/databank/img/nav/bottom_updates.gif";

var category_bottom_high = new Image();
category_bottom_high.src = "/databank/img/nav/bottom_category_high.gif";
var category_bottom = new Image();
category_bottom.src = "/databank/img/nav/bottom_category.gif";

var appearance_bottom_high = new Image();
appearance_bottom_high.src = "/databank/img/nav/bottom_appearance_high.gif";
var appearance_bottom = new Image();
appearance_bottom.src = "/databank/img/nav/bottom_appearance.gif";
